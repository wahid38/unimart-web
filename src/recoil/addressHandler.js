import { atom } from "recoil";

export const savedAddress = atom({
  key: "SAVED_ADDRESS",
  default: [],
});
