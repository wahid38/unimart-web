import { atom } from "recoil";

export const locationModal = atom({
  key: "LOCATION_MODAL",
  default: false,
});

export const locationName = atom({
  key: "LOCATION_NAME",
  default: "",
});
