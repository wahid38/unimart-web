import { atom } from "recoil";

export const ordersState = atom({
  key: "ORDERS_STATE",
  default: [],
});
