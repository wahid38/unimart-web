import { atom } from "recoil";

export const recurringUnitTime = atom({
  key: "recurringUnitTime",
  default: "week",
});

export const recurringInterval = atom({
  key: "recurringInterval",
  default: [],
});

export const recurringContinuty = atom({
  key: "recurringContinuty",
  default: 1,
});

export const isRecurring = atom({
  key: "isRecurring",
  default: false,
});
