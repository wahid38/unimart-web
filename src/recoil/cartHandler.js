import { atom } from "recoil";

export const allCartItems = atom({
  key: "allCartItems",
  default: {},
});

export const totalItems = atom({
  key: "totalItems",
  default: 0,
});

export const totalCartAmount = atom({
  key: "totalCartAmount",
  default: 0,
});
