import { atom } from "recoil";

export const subscriptionModal = atom({
  key: "SUBSCRIPTION_MODAL",
  default: false,
});

export const isChecked = atom({
  key: "IS_CHECKED",
  default: false,
});
