import axios from "axios";
import Cookies from "js-cookie";

const token = Cookies.get("user-auth");

export const axiosGet = async (url, setData) => {
  try {
    const result = await axios({
      method: "GET",
      url: url,
    });
    setData(result.data);
  } catch (error) {
    console.error(error.response);
  }
};

export const axiosGetExtra = async (url, setData, jwt) => {
  try {
    const result = await axios({
      method: "GET",
      url: url,
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });
    setData(result.data.result);
  } catch (error) {
    console.error(error.response);
  }
};

export const axiosGetOrders = async (url, setData, jwt) => {
  try {
    const result = await axios({
      method: "GET",
      url: url,
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });
    setData(result.data);
  } catch (error) {
    console.error(error.response);
  }
};
