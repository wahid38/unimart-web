import Cookies from "js-cookie";
import axios from "axios";
import { notifyError, notifySuccess } from "@utils/toast";

export const userLogin = async (data, setModal) => {
  try {
    const result = await axios({
      method: "POST",
      url: "/Authentication/customerlogin",
      data: {
        phoneNumber: data.phoneNumber,
        password: data.password,
      },
    });

    Cookies.set("user-auth", result.data.token, {
      expires: 1,
    });
    notifySuccess("Login Successful!");
    setModal(false);
  } catch (error) {
    notifyError("Phone Number or Password was incorrect!");
    setModal(false);
    console.error(error.response);
  }
};

export const userRegister = async (data, setModal) => {
  try {
    const result = await axios({
      method: "POST",
      url: "/Authentication/register-customer",
      data: {
        firstName: data.firstName,
        lastName: data.lastName,
        phoneNumber: data.phoneNumber,
        email: data.email,
        password: data.password,
      },
    });

    Cookies.set("user-auth", result.data.token, {
      expires: 1,
    });
    notifySuccess("Register Successful!");
    setModal(false);
  } catch (error) {
    notifyError("Something Went Wrong!");
    setModal(false);
    console.error(error.response);
  }
};
