import axios from "axios";
import { notifyError, notifySuccess } from "@utils/toast";

export const placeOrder = async (
  isSelect,
  paymentType,
  carts,
  selectedAddress,
  token,
  intarval = [],
  cartsAmount,
  shipping,
  coupon = "",
  afterDiscount = 0,
  monthOrWeek,
  discountAmount = 0,
  setCarts,
  setCartsItems,
  setCartsAmount,
  setCoupon,
  setAppliedCouponAmount,
  setAppliedCouponName,
  setAfterDiscount,
  couponRef,
  setTimeIntarval,
  setMonthOrWeek,
  setRecurringCont,
  setRecurring,
  continuity,
  setIsSelect,
  setSelectedAddress
) => {
  if (isSelect === null) {
    notifyError("Please Select A Delivery Address!");
    return;
  }
  if (paymentType === 0) {
    notifyError("Please Select A payment Type!");
    return;
  }

  const campItems = [];
  const orderItems = [];
  const recurringTable = [];

  // Campaign
  carts?.campaignDTOs?.forEach((el) => {
    campItems.push({
      productId: el.productId,
      productPrice: el.productPrice,
      productQuantity: el.quantity,
      campaignId: el.campaignId,
    });
  });

  // Normal
  carts?.orderProductsDTOs?.forEach((el) => {
    orderItems.push({
      productId: el.productId,
      productPrice: el.productPrice,
      productQuantity: el.quantity,
    });
  });

  // Recurring
  intarval?.forEach((el) =>
    recurringTable.push({
      isMonthOrWeek: monthOrWeek,
      monthOrDateValue: el,
      continuity: continuity,
    })
  );

  const orderData = {
    createOrderDTO: {
      voucherCode: coupon,
      totalPrice: cartsAmount,
      grandTotalPrice: cartsAmount + shipping - discountAmount,
      afterPromoPrice: coupon ? afterDiscount : 0,
      promoPrice: discountAmount,
      paymentType: paymentType,
      zoneId: selectedAddress.zoneId,
      deliveryType: 1,
      deliveryAddressID: selectedAddress.addressId,
      deliveryPrice: shipping,
      isCampaign: carts.campaignDTOs.length > 0 ? true : false,
      storeId: 7,
    },
    orderProductsDTOs: orderItems,
    campaignDTOs: campItems,
    createRecurringDTOs: recurringTable,
  };

  try {
    const result = await axios({
      method: "POST",
      url: "/Order",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: orderData,
    });
    notifySuccess("Order Placed Successfully!");

    setCarts([]);
    setCartsItems(0);
    setCartsAmount(0);
    setCoupon({});
    setAppliedCouponAmount(0);
    setAppliedCouponName("");
    setAfterDiscount(0);

    couponRef.current.value = "";

    localStorage.setItem(
      "uni-cart",
      JSON.stringify({ orderProductsDTOs: [], campaignDTOs: [] })
    );
    localStorage.setItem("uni-cart-items", JSON.stringify(0));
    localStorage.setItem("uni-cart-amount", JSON.stringify(0));

    setTimeIntarval([]);
    setMonthOrWeek("Weekly");
    setRecurringCont(1);
    setRecurring(false);

    setIsSelect(null);
    setSelectedAddress({});

    console.log(result);
  } catch (error) {
    notifyError("Something Went Wrong!");
    console.error(error.response);
  }
};
