export const camapignProduct = [
    {
        title: 'Combo Product 1',
        slug: 'combo-1',
        parent: 'Baby Care',
        children: 'Baby Accessories',
        image: ['/car5.jpg'],
        originalPrice: 8,
        price: 8,
        discount: 10,
        unit: '72pcs',
        quantity: 100,
        type: 'Health Care',
        //tag: ['baby care', 'baby accessories'],
        description:
          'Baby Products are products intended to be used on infants and children under the age of three. Baby products are specially formulated to be mild and non-irritating and use ingredients that are selected for these properties. Baby products include baby shampoos and baby lotions, oils, powders and creams.',
    },
    {
        title: 'Combo Product 2',
        slug: 'combo-2',
        parent: 'Baby Care',
        children: 'Baby Accessories',
        image: ['/jar2.jpg'],
        originalPrice: 25,
        price: 15,
        discount: 10,
        unit: '72pcs',
        quantity: 100,
        type: 'Health Care',
        //tag: ['baby care', 'baby accessories'],
        description:
          'Baby Products are products intended to be used on infants and children under the age of three. Baby products are specially formulated to be mild and non-irritating and use ingredients that are selected for these properties. Baby products include baby shampoos and baby lotions, oils, powders and creams.',
    },
]