import Cookies from "js-cookie";
import * as dayjs from "dayjs";
import { useRouter } from "next/router";
import { useContext, useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { useCart } from "react-use-cart";
import { useRecoilState } from "recoil";
//import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";

//internal import
// import useAsync from "@hooks/useAsync";
import { UserContext } from "@context/UserContext";

import { notifyError, notifySuccess } from "@utils/toast";
import { savedAddress } from "src/recoil/addressHandler";

const useCheckoutSubmit = () => {
  const {
    state: { userInfo, shippingAddress },
    dispatch,
  } = useContext(UserContext);
  //console.log("value:", shippingAddress, "find");

  const [error, setError] = useState("");
  const [total, setTotal] = useState("");
  const [couponInfo, setCouponInfo] = useState({});
  const [minimumAmount, setMinimumAmount] = useState(0);
  const [showCard, setShowCard] = useState(false);
  const [shippingCost, setShippingCost] = useState(0);
  const [discountAmount, setDiscountAmount] = useState(0);
  const [discountPercentage, setDiscountPercentage] = useState(0);
  const [isCheckoutSubmit, setIsCheckoutSubmit] = useState(false);
  const [isSaved, setIsSaved] = useRecoilState(savedAddress);

  const router = useRouter();
  //const stripe = useStripe();
  //const elements = useElements();
  const couponRef = useRef("");
  const { isEmpty, emptyCart, items, cartTotal } = useCart();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    if (Cookies.get("couponInfo")) {
      const coupon = JSON.parse(Cookies.get("couponInfo"));
      setCouponInfo(coupon);
      setDiscountPercentage(coupon.discountPercentage);
      setMinimumAmount(coupon.minimumAmount);
    }
  }, []);

  //remove coupon if total value less then minimum amount of coupon
  useEffect(() => {
    if (minimumAmount - discountAmount > total || isEmpty) {
      setDiscountPercentage(0);
      Cookies.remove("couponInfo");
    }
  }, [minimumAmount, total]);

  //calculate total and discount value
  useEffect(() => {
    let totalValue = "";
    let subTotal = (cartTotal + shippingCost).toFixed(2);
    let discountAmount = subTotal * (discountPercentage / 100);
    totalValue = subTotal - discountAmount;
    setDiscountAmount(discountAmount);
    setTotal(totalValue);
  }, [cartTotal, shippingCost, discountPercentage]);

  //if not login then push user to home page
  useEffect(() => {
    // if (!userInfo) {
    //   router.push("/");
    // }

    setValue("name", shippingAddress.name);
    setValue("house", shippingAddress.house);
    setValue("address", shippingAddress.address);
    setValue("phone", shippingAddress.phone);
    setValue("road", shippingAddress.road);
    setValue("area", shippingAddress.area);
    setValue("division", shippingAddress.division);
    setValue("district", shippingAddress.district);
    setValue("zone", shippingAddress.zone);
    setValue("saveAs", shippingAddress.saveAs);
  }, []);

  const submitHandler = async (data) => {
    // console.log(data);
    dispatch({ type: "SAVE_SHIPPING_ADDRESS", payload: data });
    Cookies.set("shippingAddress", JSON.stringify(data));
    setIsCheckoutSubmit(true);

    let userAddress = {
      name: data.name,
      houseNo: data.house,
      detailAddress: data.address,
      district: data.district,
      zip: data.zone,
      contactNumber: data.phone,
      roadNo: data.road,
      zone: data.zone,
      division: data.division,
      saveAddressAs: data.saveAs,
    };
    // console.log(userAddress);
    let orderInfo = {
      name: data.name,
      address: data.address,
      phone: data.phone,
      house: data.house,
      road: data.road,
      division: data.division,
      district: data.district,
      zone: data.zone,
      saveAs: data.saveAs,
      area: data.area,
      shippingOption: data.shippingOption,
      paymentMethod: data.paymentMethod,
      status: "Pending",
      cart: items,
      subTotal: cartTotal,
      shippingCost: shippingCost,
      discount: discountAmount,
      total: total,
    };
    //Console log data
    //console.log(orderInfo);
    setIsSaved([...isSaved, { ...orderInfo }]);
    notifySuccess("Your Address Saved Successfully");
  };

  const handleShippingCost = (value) => {
    setShippingCost(value);
  };

  const handleCouponCode = (e) => {
    e.preventDefault();

    if (!couponRef.current.value) {
      notifyError("Please Input a Coupon Code!");
      return;
    }
    const result = data.filter(
      (coupon) => coupon.couponCode === couponRef.current.value
    );

    if (result.length < 1) {
      notifyError("Please Input a Valid Coupon!");
      return;
    }

    if (dayjs().isAfter(dayjs(result[0]?.endTime))) {
      notifyError("This coupon is not valid!");
      return;
    }

    if (total < result[0]?.minimumAmount) {
      notifyError(
        `Minimum ${result[0].minimumAmount} USD required for Apply this coupon!`
      );
      return;
    } else {
      notifySuccess("Coupon is Applied!");
      setDiscountPercentage(result[0].discountPercentage);
      setMinimumAmount(result[0]?.minimumAmount);
      dispatch({ type: "SAVE_COUPON", payload: result[0] });
      Cookies.set("couponInfo", JSON.stringify(result[0]));
    }
  };

  return {
    handleSubmit,
    submitHandler,
    handleShippingCost,
    register,
    errors,
    showCard,
    setShowCard,
    error,
    //stripe,
    couponInfo,
    couponRef,
    handleCouponCode,
    discountPercentage,
    discountAmount,
    shippingCost,
    total,
    isEmpty,
    items,
    cartTotal,
    isCheckoutSubmit,
  };
};

export default useCheckoutSubmit;
