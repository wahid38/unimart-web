import React, { useContext } from "react";
import Cookies from "js-cookie";
import { useForm } from "react-hook-form";
import { useRecoilState } from "recoil";
import { locationModal, locationName } from "src/recoil/locationHandler";
import { UserContext } from "@context/UserContext";
import axios from "axios";

const useLocationSubmit = () => {
  const [modalOpen, setModalOpen] = useRecoilState(locationModal);
  const [location, setLocation] = useRecoilState(locationName);

  //Context hook
  const {
    state: { storeId },
    dispatch,
  } = useContext(UserContext);
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const submitHandler = async (result) => {
    const id = result.location[0];
    const area = result.location.substr(2);
    console.log(area);
    console.log(id);
    dispatch({ type: "SAVE_STORE_ID", payload: result });
    Cookies.set("storeId", JSON.stringify(id));
    Cookies.set("location", JSON.stringify(area));
    setLocation(area);
    setModalOpen(!modalOpen);

    axios
      .get(
        `https://test.bespokeit.io/api/Products/getproductsbycategories/${id}`
      )
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  return {
    handleSubmit,
    submitHandler,
    register,
    errors,
  };
};

export default useLocationSubmit;
