import { useContext, useEffect, useState } from "react";
import Cookies from "js-cookie";
import Link from "next/link";
import Image from "next/image";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { useCart } from "react-use-cart";
import { IoSearchOutline } from "react-icons/io5";
import { FiShoppingCart, FiUser, FiBell } from "react-icons/fi";
import { MdOutlineFavoriteBorder } from "react-icons/md";
import { useRecoilValue, useRecoilState } from "recoil";
import { locationModal, locationName } from "src/recoil/locationHandler";

//internal import
import NavbarPromo from "@layout/navbar/NavbarPromo";
import { UserContext } from "@context/UserContext";
import LoginModal from "@component/modal/LoginModal";
import CartDrawer from "@component/drawer/CartDrawer";
import { SidebarContext } from "@context/SidebarContext";
import logo from "./rsz_1unimart_logo_.png";
import { totalItems } from "src/recoil/cartHandler";

const Navbar = () => {
  // const [imageUrl, setImageUrl] = useState("");
  const [searchText, setSearchText] = useState("");
  const [modalOpen, setModalOpen] = useState(false);
  const [isModalOpen, setIsModalOpen] = useRecoilState(locationModal);
  const area = useRecoilValue(locationName);
  const { toggleCartDrawer } = useContext(SidebarContext);
  const cartsItems = useRecoilValue(totalItems);
  const router = useRouter();

  const {
    state: { userInfo, location, storeId },
  } = useContext(UserContext);

  const token = Cookies.get("user-auth");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (searchText) {
      router.push(
        `/search/${searchText}?tittle=${searchText
          .toLowerCase()
          .replace("&", "")
          .split(" ")
          .join("-")}`,
        null,
        { scroll: false }
      );
      setSearchText("");
    } else {
      router.push(`/ `, null, { scroll: false });
      setSearchText("");
    }
  };

  // useEffect(() => {
  //   if (Cookies.get("userInfo")) {
  //     const user = JSON.parse(Cookies.get("userInfo"));
  //     setImageUrl(user.image);
  //   }
  // }, []);

  return (
    <>
      <CartDrawer />
      {modalOpen && (
        <LoginModal modalOpen={modalOpen} setModalOpen={setModalOpen} />
      )}

      <div className="bg-white  sticky top-0 z-20">
        <div className="border-b border-solid border-green-600  max-w-screen-2xl mx-auto px-3 sm:px-10">
          <div className="top-bar h-16 lg:h-auto flex items-center justify-between py-1 mx-auto">
            <Link href="/">
              <a
                className="mr-0 flex items-center lg:mr-0 xl:mr-0"
                style={{ position: "relative", left: "-15px" }}
              >
                <Image
                  width={220}
                  height={70}
                  src={logo}
                  alt="logo"
                  //layout="responsive"
                  objectFit="cover"
                />
              </a>
            </Link>
            <div className="w-full transition-all duration-200 ease-in-out lg:flex lg:max-w-[520px] xl:max-w-[750px] 2xl:max-w-[900px] md:mx-6 lg:mx-4 xl:mx-0">
              <div className="w-full flex flex-col justify-center flex-shrink-0 relative z-30">
                <div className="flex flex-col mx-auto w-full">
                  <form
                    onSubmit={handleSubmit}
                    className="relative pr-0 md:pr-0 bg-white overflow-hidden shadow-sm rounded-md w-full border border-solid border-green-600"
                  >
                    {/* <label className="flex items-center py-0.5"> */}
                    <input
                      onChange={(e) => setSearchText(e.target.value)}
                      value={searchText}
                      className="form-input w-full pl-2 appearance-none transition ease-in-out border text-input text-sm font-sans rounded-md min-h-6 h-7 md:h-7 lg:h-8 duration-200 bg-white focus:ring-0 outline-none border-none focus:outline-none placeholder-gray-500 placeholder-opacity-75"
                      placeholder="Search for products"
                    />
                    {/* </label> */}
                    <button
                      aria-label="Search"
                      type="submit"
                      className="outline-none text-xl lg:text-2xl text-green-600 absolute top-0 right-0 end-0 w-6 md:w-8 h-full flex items-center justify-center transition duration-200 ease-in-out hover:text-heading focus:outline-none"
                    >
                      <IoSearchOutline />
                    </button>
                  </form>
                </div>
              </div>
            </div>
            <div className="md:items-center lg:flex lg:justify-center lg:items-center  xl:flex inset-y-0 right-0 top-2 pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
              {location && (
                <button
                  className="text-xs md:text-sm xl:text-base font-serif font-medium border border-solid border-green-600 cursor-pointer ml-2 rounded-xl px-1 md:px-2 py-0.5 "
                  aria-label="Alert"
                  style={{ color: "#fb4512" }}
                  onClick={() => setIsModalOpen(!isModalOpen)}
                >
                  {area ? area : location}
                </button>
              )}
            </div>
            <div className="hidden md:hidden md:items-center lg:flex lg:justify-center lg:items-center  xl:flex absolute inset-y-0 right-0 top-2 pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
              <Link href="/wish-list" passHref>
                <button
                  className="pr-5 text-green-600 text-2xl font-normal"
                  aria-label="Alert"
                >
                  <MdOutlineFavoriteBorder className="w-6 h-6 drop-shadow-xl" />
                </button>
              </Link>
              <button
                aria-label="Total"
                onClick={toggleCartDrawer}
                className="relative px-5 text-green-600 text-2xl font-normal"
              >
                <span
                  style={{ background: "#fb4512" }}
                  className="absolute z-10 top-0 right-0 inline-flex items-center justify-center p-1 h-5 w-5 text-xs font-medium leading-none text-red-100 transform -translate-x-1/2 -translate-y-1/2 rounded-full"
                >
                  {cartsItems}
                </span>
                <FiShoppingCart className="w-6 h-6 drop-shadow-xl" />
              </button>
              {/* Profile dropdown */}

              <button
                className="pl-5 text-green-600 text-2xl font-normal"
                aria-label="Login"
              >
                {token ? (
                  <Link href="/user/dashboard">
                    <span>
                      <FiUser className="w-6 h-6 drop-shadow-xl" />
                    </span>
                  </Link>
                ) : (
                  <span onClick={() => setModalOpen(!modalOpen)}>
                    <FiUser className="w-6 h-6 drop-shadow-xl" />
                  </span>
                )}
              </button>
            </div>
          </div>
        </div>

        {/* second header */}
        <NavbarPromo />
      </div>
    </>
  );
};
export default dynamic(() => Promise.resolve(Navbar), { ssr: false });
