import React from "react";
import Link from "next/link";
import Image from "next/image";

const FooterTop = () => {
  return (
    <div className="max-w-screen-2xl mt-0 mx-auto bg-white ">
      {/* Partner Banner */}

      <Image
        src="/Partner_Banner.jpg"
        alt="Partner Banner"
        width="100%"
        height="30%"
        layout="responsive"
        objectFit="cover"
      />

      {/* Site Overview */}
      <div className="grid md:grid-cols-3 lg:grid-cols-3 gap-5 lg:gap-10 md:px-10 lg:px-10 mx-auto max-w-screen-2xl">
        <div className="mx-5 my-5 sm:mx-10 md:mx-0 relative">
          <div
            className="w-full h-80 bg-no-repeat bg-cover bg-right-bottom object-cover"
            style={{
              backgroundImage:
                "linear-gradient(-5deg, #FB2D15 54.5%, transparent 55%), url('/career_image.jpg')",
            }}
          ></div>
          <div className="absolute left-0 bottom-0  w-full pb-5 px-5">
            <h3 className="text-lg text-white capitalize font-semibold font-serif my-1">
              News & Blogs
            </h3>
            <p className="font-sans text-white text-sm">
              Staying healthy is a blessing.
              <br />
              To enhance your knowledge of being healthy...
            </p>
            {/* <button type='button' className='bg-green-500 border-none text-sm font-serif text-white'>Read with us</button> */}
            <div className="mt-2">
              <Link href="/">
                <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-2  py-2 md:px-3 md:py-2 hover:text-white hover:bg-green-600  text-xs lg:text-sm ">
                  Read with us
                </a>
              </Link>
            </div>
          </div>
        </div>

        {/*  */}
        <div className="mx-5 my-5 sm:mx-10 md:mx-0 relative">
          <div
            className="w-full h-80 bg-no-repeat bg-cover bg-right-bottom object-cover"
            style={{
              backgroundImage:
                "linear-gradient(-5deg, #FB8624 54.5%, transparent 55%), url('/career_image.jpg')",
            }}
          ></div>
          <div className="absolute left-0 bottom-0  w-full pb-5 px-5">
            <h3 className="text-lg text-white capitalize font-semibold font-serif my-1">
              News & Blogs
            </h3>
            <p className="font-sans text-white text-sm">
              Staying healthy is a blessing.
              <br />
              To enhance your knowledge of being healthy...
            </p>
            {/* <button type='button' className='bg-green-500 border-none text-sm font-serif text-white'>Read with us</button> */}
            <div className="mt-2">
              <Link href="/">
                <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-2  py-2 md:px-3 md:py-2 hover:text-white hover:bg-green-600  text-xs lg:text-sm ">
                  Read with us
                </a>
              </Link>
            </div>
          </div>
        </div>

        {/*  */}

        <div className="mx-5 my-5 sm:mx-10 md:mx-0 relative">
          <div
            className="w-full h-80 bg-no-repeat bg-cover bg-right-bottom object-cover"
            style={{
              backgroundImage:
                "linear-gradient(-5deg, #9F9F9F 54.5%, transparent 55%), url('/career_image.jpg')",
            }}
          ></div>
          <div className="absolute left-0 bottom-0  w-full pb-5 px-5">
            <h3 className="text-lg text-white capitalize font-semibold font-serif my-1">
              News & Blogs
            </h3>
            <p className="font-sans text-white text-sm">
              Staying healthy is a blessing.
              <br />
              To enhance your knowledge of being healthy...
            </p>
            {/* <button type='button' className='bg-green-500 border-none text-sm font-serif text-white'>Read with us</button> */}
            <div className="mt-2">
              <Link href="/">
                <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-2  py-2 md:px-3 md:py-2 hover:text-white hover:bg-green-600  text-xs lg:text-sm ">
                  Read with us
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/* end */}
      {/* <div className="max-w-screen-2xl mx-auto px-4 sm:px-10 flex flex-col md:flex-row lg:flex-row lg:items-center">
        <div className="flex-shrink-0 lg:w-2/5 md:w-3/6 py-10 lg:py-0">
          <h3 className="text-xl md:text-2xl lg:text-3xl font-bold font-serif mb-3">
            Get Your Daily Needs From Our KachaBazar Store
          </h3>
          <p className="text-base opacity-90 leading-7">
            There are many products you will find our shop, Choose your daily
            necessary product from our KachaBazar shop and get some special
            offer.
          </p>
          <div className="mt-8">
            <Link href="https://www.apple.com/app-store/">
              <a className="mr-3" target="_blank" rel="noreferrer">
                <Image
                  width={170}
                  height={50}
                  className="mr-2 rounded"
                  src="/app/app-store.svg"
                  alt="app store"
                />
              </a>
            </Link>
            <Link href="https://play.google.com/store/apps">
              <a target="_blank" rel="noreferrer">
                <Image
                  width={170}
                  height={50}
                  className="rounded"
                  src="/app/play-store.svg"
                  alt="app store"
                />
              </a>
            </Link>
          </div>
        </div>
        <div className="flex-grow hidden lg:flex md:flex lg:justify-end">
          <Image
            width={500}
            height={394}
            src="/app-download-img.png"
            alt="app download"
            className="block w-auto"
          />
        </div>
      </div> */}
    </div>
  );
};

export default FooterTop;
