import React, { useContext, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import dynamic from "next/dynamic";
import { useCart } from "react-use-cart";
import Cookies from "js-cookie";
import {
  FiHome,
  FiUser,
  FiShoppingCart,
  FiAlignLeft,
  FiPercent,
} from "react-icons/fi";
import { MdOutlineFavoriteBorder } from "react-icons/md";
import { useRecoilValue } from "recoil";

import { totalItems } from "src/recoil/cartHandler";
import { UserContext } from "@context/UserContext";
import LoginModal from "@component/modal/LoginModal";
import { SidebarContext } from "@context/SidebarContext";
import CategoryDrawer from "@component/drawer/CategoryDrawer";

const MobileFooter = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const { toggleCartDrawer, toggleCategoryDrawer } = useContext(SidebarContext);
  //const { totalItems } = useCart();
  const cartsItems = useRecoilValue(totalItems);

  const {
    state: { userInfo },
  } = useContext(UserContext);
  const token = Cookies.get("user-auth");

  return (
    <>
      <LoginModal modalOpen={modalOpen} setModalOpen={setModalOpen} />
      <div className="flex flex-col h-full justify-between align-middle bg-white rounded cursor-pointer overflow-y-scroll flex-grow scrollbar-hide w-3/4">
        <CategoryDrawer className="w-6 h-6 drop-shadow-xl" />
      </div>
      <footer className="lg:hidden fixed z-30 bottom-0 bg-green-500 flex items-center justify-between w-full h-14 px-3 sm:px-10">
        <button
          aria-label="Bar"
          onClick={toggleCategoryDrawer}
          className="flex items-center justify-center flex-shrink-0 h-auto relative focus:outline-none"
        >
          <span className="text-xl text-white">
            <FiAlignLeft className="w-5 h-5 drop-shadow-xl" />
          </span>
        </button>
        <Link href="/offer">
          <a className="text-xl text-white" rel="noreferrer" aria-label="Home">
            {" "}
            <FiPercent className="w-5 h-5 drop-shadow-xl" />
          </a>
        </Link>

        <Link href="/wish-list">
          <a className="text-xl text-white" rel="noreferrer" aria-label="Home">
            {" "}
            <MdOutlineFavoriteBorder className="w-5 h-5 drop-shadow-xl" />
          </a>
        </Link>

        <button
          onClick={toggleCartDrawer}
          className="h-9 w-9 relative whitespace-nowrap inline-flex items-center justify-center text-white text-lg"
        >
          <span className="absolute z-10 top-0 right-0 inline-flex items-center justify-center p-1 h-5 w-5 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 bg-red-500 rounded-full">
            {cartsItems}
          </span>
          <FiShoppingCart className="w-5 h-5 drop-shadow-xl" />
        </button>
        <button
          aria-label="User"
          type="button"
          className="text-xl text-white indicator justify-center"
        >
          {token ? (
            <Link href="/user/dashboard">
              <span>
                <FiUser className="w-6 h-6 drop-shadow-xl" />
              </span>
            </Link>
          ) : (
            <span onClick={() => setModalOpen(!modalOpen)}>
              <FiUser className="w-5 h-5 drop-shadow-xl" />
            </span>
          )}
        </button>
      </footer>
    </>
  );
};

export default dynamic(() => Promise.resolve(MobileFooter), { ssr: false });
