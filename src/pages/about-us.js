import React from "react";
import Image from "next/image";
import ReactPlayer from "react-player";

//internal import
import Layout from "@layout/Layout";
import PageHeader from "@component/header/PageHeader";

const AboutUs = () => {
  return (
    <Layout title="About Us" description="This is about us page">
      <PageHeader title="About Us" />

      <div className="bg-white">
        <div className="max-w-screen-2xl mx-auto lg:py-20 py-10 px-3 sm:px-10">
          <div className="mb-8 lg:mb-12 last:mb-0 text-center">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              UNIMART
            </h2>
            <h2 className="text-xl xl:text-xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              THE ULTIMATE RETAIL EXPERIENCE
            </h2>
            <div className="font-sans leading-7">
              <p>
                UNIMART LTD., a concern of United Group, is one of the premium
                most super shops of Bangladesh that meets all your domestic
                shopping requirements. Having two major outlets at most
                lucrative locations in Dhaka, Unimart is now ready to serve you
                online.
              </p>
              <p>
                With diverse commercial venturing, United Group first
                introduced, Unimart as a single floor compact super market
                covering an area of 40,000 sft and it is the first of its kind
                retail format accommodating food, nonfood and general
                merchandise along with store-in-store counters.
              </p>
              <p>
                After almost 6 years of starting its service, Unimart Limited
                took the ultimate retail experience and comfort to Dhanmondi. A
                3-storey super shop covering an area of 30,000 sft, this outlet
                also has the widest range of products along with many other
                benefits. Unimart has two express outlets within the premises of
                United Hospital Limited and United International University
                (UIU).
              </p>
              <p>
                Each month nearly 200,000 customers of the premium most segments
                of the city is served by Unimart.
              </p>
              <p>
                Unimart is the super shop, which is not just for Pick-&-Buy, it
                offers Browse-&-Shop Experience to the consumers. Thanks to the
                large number of Suppliers we have, we can offer the best
                products to our consumer without compromising the quality. Our
                quality of service is our key strength.
              </p>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default AboutUs;
