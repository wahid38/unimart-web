import React from "react";
import Link from "next/link";

//internal import
import Layout from "@layout/Layout";
import PageHeader from "@component/header/PageHeader";

const TermAndConditions = () => {
  return (
    <Layout
      title="Terms & Conditions"
      description="This is terms and conditions page"
    >
      <PageHeader title="Terms & Conditions" />
      <div className="bg-white">
        <div className="max-w-screen-2xl mx-auto lg:py-20 py-10 px-3 sm:px-10">
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              General Terms & Conditions
            </h2>
            <div className="font-sans leading-7">
              <p>
                Unimart Online is a shopping platform where you can browse,
                select and place orders by just a few clicks.
              </p>
            </div>
          </div>
          <div className="mb-6 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              Agreement to the terms and conditions
            </h2>
            <div className="font-sans leading-7">
              <p>
                By agreeing, accessing and using the site, including placing
                orders of products through the site, you agree that you will be
                subject to and will comply with the terms and conditions
              </p>
              <p>(a) By completing your registration through the site; and</p>
              <p>(b) Using the site to obtain products from us.</p>
            </div>
          </div>
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              Registration
            </h2>
            <div className="font-sans leading-7">
              <p>
                You must complete the customer registration process through the
                site before placing an order for products through the site.
              </p>
              <p>
                You may not have more than one active account, and your account
                is non-transferable. You may update, edit or terminate your
                account at any time through the site. You may not have more than
                two accounts with different delivery addresses.
              </p>
              <p>
                If you choose to use a workplace email address for your account
                or to access the site, then you are solely responsible for
                ensuring that you comply with the rules, policies or protocols
                that apply to the use of your email address and your workplace
                facilities.
              </p>
              <p>
                It is advised to log-in with your delivery location to avoid any
                kind of confusion for delivery places.
              </p>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Product Promotions Policy
              </h2>
              <div className="font-sans leading-7">
                <p>
                  Please note that the delivery of promotional products or
                  products with a special offer is subject to stock
                  availability. If the product is out of stock, then we request
                  you to replace it with another product which is in stock. You
                  may also cancel the order if you want.
                </p>
              </div>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Order Policy
              </h2>
              <div className="font-sans leading-7">
                <p>
                  There will be order limits set for certain products to ensure
                  every customer can avail of the necessary products for daily
                  consumption. Unimart Online is not a marketplace for business
                  to business (B2B) transactions. It is instead an online
                  shopping platform for personal consumption and usage. Thus, we
                  highly discourage shopping with reselling or commercial
                  motives.
                </p>
              </div>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Pricing, Availability and Order Processing
              </h2>
              <div className="font-sans leading-7">
                <p>
                  All prices are listed in Bangladeshi Taka (BDT) and are
                  inclusive of VAT and are listed on the Site by the seller that
                  is selling the product or service. Items in your Shopping Cart
                  will always reflect the most recent price displayed on the
                  item's product detail page. Please note that this price may
                  differ from the price shown for the item when you first placed
                  it in your cart. Placing an item in your cart does not reserve
                  the price shown at that time. It is also possible that an
                  item's price may decrease between the time you place it in
                  your basket and the time you purchase it.
                </p>
                <p>
                  We do not offer price matching for any items sold by any
                  seller on our Site or other websites.
                </p>
                <p>
                  We are determined to provide the most accurate pricing
                  information on the Site to our users; however, errors may
                  still occur, such as cases when the price of an item is not
                  displayed correctly on the Site. As such, we reserve the right
                  to refuse or cancel any order. In the event that an item is
                  mispriced, we may, at our own discretion, either contact you
                  for instructions or cancel your order and notify you of such
                  cancellation. We shall have the right to refuse or cancel any
                  such orders whether or not the order has been confirmed and
                  your prepayment processed. If such a cancellation occurs on
                  your prepaid order, our policies for a refund will apply.
                  Please note that Unimart possesses 100% right on the refund
                  amount. Usually, the refund amount is calculated based on the
                  customer paid price after deducting any sort of discount and
                  shipping fee.
                </p>
                <p>
                  We list availability information for products listed on the
                  Site, including on each product information page. Beyond what
                  we say on that page or otherwise on the Site, we cannot be
                  more specific about availability. Please note that dispatch
                  estimates are just that. They are not guaranteed dispatch
                  times and should not be relied upon as such. As we process
                  your order, you will be informed by a phone call or e-mail or
                  SMS if any products you order turn out to be unavailable.
                </p>
                <p>
                  Please note that there are cases when an order cannot be
                  processed for various reasons. The Site reserves the right to
                  refuse or cancel any order for any reason at any given time.
                  You may be asked to provide additional verifications or
                  information, including but not limited to phone number and
                  address before we accept the order.
                </p>
                <p>
                  In order to avoid any fraud with credit or debit cards, we
                  reserve the right to obtain validation of your payment details
                  before providing you with the product and to verify the
                  personal information you shared with us. This verification can
                  take the shape of an identity, place of residence, or banking
                  information check. The absence of an answer following such an
                  inquiry will automatically cause the cancellation of the order
                  within a reasonable timeline. We reserve the right to proceed
                  to direct cancellation of an order for which we suspect a risk
                  of fraudulent use of banking instruments or other reasons
                  without prior notice or any subsequent legal liability.
                </p>
              </div>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Placing an Order for Products
              </h2>
              <div className="font-sans leading-7">
                <p>
                  You may order products by selecting and submitting your order
                  through the site.
                </p>
                <p>
                  Any order placed through this site for a product to purchase
                  the particular product for the price notified (including the
                  delivery and other charges and taxes) at the time you place
                  the order.
                </p>
                <p>
                  We may ask you to provide additional details or require you to
                  confirm your details to enable us to process any orders placed
                  through the site.
                </p>
                <p>
                  You agree to provide us with current, complete and accurate
                  details when asked to do so by the site.
                </p>
              </div>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Delivery of Products
              </h2>
              <div className="font-sans leading-7">
                <p>
                  We will only deliver products ordered through the site to the
                  specific location only within our delivery services. You may
                  obtain further information on the site about our delivery time
                  frames and how we deliver your products.
                </p>
                <p>
                  The delivery of any product is subject to stock availability.
                  If the product is out of stock, then we request you to replace
                  it with another product which is in stock. You may also cancel
                  the order if you want.
                </p>
                <p>
                  We will deliver the products to the front door at the relevant
                  delivery address. If you ask us to deliver inside a premise or
                  building at the delivery address, our delivery concern
                  persons’ are strictly instructed not to go inside or any place
                  except the front door.
                </p>
                <p>
                  If you are not the person collecting your ordered items, then
                  your representative must provide us with proof of their
                  identity. You acknowledge that we may record the details of
                  any identification provided in relation to the collection of
                  products.
                </p>
              </div>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Cancellation Policy
              </h2>
              <div className="font-sans leading-7">
                <p>
                  We reserve the right to accept or reject your order for any
                  reason, including if the requested product is not available if
                  there is an error in the price or the product description
                  posted on the site or an error in your order. We also reserve
                  the right to cancel any order without giving any reason(s). We
                  also have the right to suspend or remove a user account
                  without giving any notice or justification to the user. We
                  will keep every unverified order for 6 (six) hours and make
                  several attempts to verify. If there is no response, after
                  that time the order will be automatically canceled.
                </p>
              </div>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Return Policy
              </h2>
              <div className="font-sans leading-7">
                <p>The following are the conditions for product returns:</p>
                <p>
                  1. Customers can return the grocery products within the same
                  day from the purchase date; only if the defective product is
                  delivered. We highly recommend checking before acknowledging
                  the delivery from the delivery person.
                </p>
                <p>
                  2. Any type of cosmetics, undergarments and perishable items
                  such as fish, meat, vegetable, egg, any type of dairy items,
                  liquid milk, curd, sweets, cheese, ice cream, any type of
                  loose grains (rice, lentil, dal, flour, sugar, chola) ARE NOT
                  RETURNABLE AFTER DELIVERY. Once the customer received
                  considered it sold in acceptable condition.
                </p>
                <p>
                  3. We encourage our customers to cross-check each and every
                  item while the delivery man is present at your premises. Any
                  claim regarding replacement or Return or Refund issue should
                  be final before our delivery man during delivery time. After
                  leaving your premises, Unimart Online authority will not be
                  liable for any future claim.
                </p>
                <p>
                  4. After customer consent or signature at the delivery slip;
                  Unimart Online has the right to cancel any refund or return
                  request.
                </p>
                <p>
                  5. Customers can return the products (other than the
                  perishable items) to the nearest Unimart Outlet (Gulshan &
                  Dhanmondi) within the same day receiving the product.
                </p>
                <p>
                  6. Customer can exchange products such as fashion (except
                  undergarments), sportswear, fitness, and household products
                  within 3 days from the purchase date. The exchangeable
                  products should be in packed as delivered and selling
                  conditions to any other customer. No exchange will be allowed
                  if any package or product unsealed, partially broken, torn, an
                  inadequate condition or any other damage noticed.
                </p>
                <p>
                  7. If a product found broken, unwrapped or used; then the
                  customer must inform the Customer Service immediately after
                  receiving it.
                </p>
                <p>
                  8. Fashion products can be tried on to see if they fit and
                  will still be considered unworn. If a product is returned to
                  us in an inadequate condition, we reserve the right to send it
                  back to you.
                </p>
                <p>
                  9. For after-sales service-related issue on any electrical and
                  electronics items, the warranty policy/terms & conditions of
                  the concerned company/brand/service company will be
                  applicable. The product must include the original tags, user
                  manual, warranty cards, freebies, and/or accessories. In this
                  case, Unimart Online will not be liable.{" "}
                </p>
                <p>
                  10.Any returnable or exchangeable product must be unused,
                  unwashed and without any flaws.
                </p>
                <p>
                  11.The product must be returned in the original and undamaged
                  manufacturer packaging/box. If the product was delivered in
                  the second layer of Unimart Online packaging, it must be
                  returned in the same condition with the return shipping label
                  attached.
                </p>
                <p>
                  12.We encourage our customers to cross check each and every
                  item while the delivery man is present at your premises if the
                  customer is not receiving the products then the responsible
                  person who is receiving the product is responsible to give
                  signature at the delivery slip.
                </p>
                <p>
                  13.If the ordered product quantity is mismatched / excess /
                  less / items not exactly ordered, we request you to notify us
                  within 24 hours from the receiving time.
                </p>
                <p>
                  14.The customer must contact the Customer Service Hotline
                  0196655088 (Gulshan Outlet) & 01966880099 (Dhanmondi Outlet)
                  between 10 am to 4 pm daily for any kind of return/exchange if
                  required.
                </p>
                <p>
                  15.Unimart Online reserves all the rights to decide for any
                  return or refund for any kind of request.
                </p>
              </div>
            </div>
            <div className="my-6 lg:my-8 last:mb-0">
              <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
                Refund Policy
              </h2>
              <div className="font-sans leading-7">
                <p>
                  16.If you have made online pre-payment/card payment against
                  your order, and there are product/s which is/are not available
                  in stock, we will offer you product replacement options with
                  the same price range or increased price (if any). The
                  additional amount can be paid in paid during delivery. You can
                  also have the option to take a refund through your card or
                  Bkash or adjustment voucher which can be used in future orders
                  at Unimart.online platform). If for any reason, you decline to
                  take product replacements, then the items will be dropped from
                  the list, otherwise, the order will be canceled in full and we
                  will start the refund process.
                </p>
                <p>
                  17.Please make sure to make your refund request by calling our
                  Customer Service mobile 01966550088 (Gulshan Outlet) &
                  01966880099 (Dhanmondi Outlet) between 10 am to 6 pm every
                  day. You may need to send an email request to
                  help.unimartonline@gmail.com for a refund. Unimart Online will
                  retain full right over the refund payment policy.
                </p>
                <p>
                  The refund process will usually take approximately up to 15
                  working days from the date of refund request in written to
                  complete the refund via Card. If there is any dispute /
                  inappropriate amount of claim, it may take more days to settle
                  the refund.
                </p>
                <p>
                  The refund via mobile banking may take up to 3-5 working days
                  from the date of the refund request is made.
                </p>
                <p>
                  No cash refund is allowed for a refund if the payment made by
                  any pre-paid card/debit card/credit card or any digital
                  payment gateway.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default TermAndConditions;
