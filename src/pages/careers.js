import React from "react";
import Layout from "@layout/Layout";
import PageHeader from "@component/header/PageHeader";
import Image from "next/image";

// import useFilter from "@hooks/useFilter";
// import { products } from "@utils/products";
// import ProductCard from "@component/product/ProductCard";

const WishList = () => {
  //const { productData } = useFilter(products);

  return (
    <Layout title="Careers" description="This is careers page">
      <PageHeader title="Careers" />
      {/*  */}
      <div className="max-w-screen-2xl mx-auto lg:py-0 py-0 px-0 sm:px-0">
        <div className="grid grid-flow-row lg:grid-cols-2 gap-2 lg:gap-0 items-center">
          <div className="mt-0 lg:mt-0">
            <Image
              width={920}
              height={750}
              src="/about_us/career.jpg"
              alt="About Us Image"
              layout="responsive"
            />
          </div>
          <div className="px-10 lg:px-10 lg:pt-0">
            <h3 className="text-xl text-center lg:text-2xl mb-2 font-serif font-semibold">
              Life At Farmroots
            </h3>
            <div className="mt-3 text-base md:text-lg lg:text-base xl:text-base font-serif opacity-90">
              <p>
                " We do not work here, we enjoy our time to make a Healthier &
                Happier World."
              </p>
            </div>
          </div>
        </div>
        <div className="mt-0 h-20 lg:mt-0 flex flex-col bg-green-500 text-white text-center sm:grid gap-4 justify-center items-center">
          <h2 className="text-lg sm:text-2xl uppercase font-serif font-semibold px-5 sm:px-10">
            What we promise - We Deliver
          </h2>
        </div>
      </div>
      {/*  */}
      <div className="max-w-screen-2xl mx-auto lg:py-0 py-0 px-0 sm:px-0">
        <div className="grid grid-flow-row lg:grid-cols-2 gap-0 lg:gap-0 items-center">
          <div className="px-10 lg:px-0 pt-5 lg:pt-0 lg:pl-10">
            <h3 className="text-2xl text-left lg:text-3xl mb-2 font-serif font-semibold py-5 lg:py-10">
              Job Opening
            </h3>
            <div className="mt-3 text-base md:text-lg lg:text-base xl:text-base font-serif opacity-90">
              {/* <p>
                "We want to become the example of keeping people healthy,
                providing pure food products and doing a sustainable business at
                the same time. We'll be Big in terms of Brand Value, not
                necessarily in terms of size of the business."
              </p>

              <p>
                "Adulterated food production and its mass marketing is driving
                the nation’s future towards an uncertainty. Farmroots is born to
                break down this virtual wall."
              </p> */}
            </div>
          </div>
          {/* <div className="mt-10 lg:mt-0">
            <Image
              width={920}
              height={750}
              src="/about_us/image2.jpg"
              alt="About Us Image"
              layout="responsive"
            />
          </div> */}
        </div>
        {/* <div className="mt-0 h-20 lg:mt-0 flex flex-col bg-green-500 text-white text-center sm:grid gap-4 justify-center items-center">
          <h2 className="text-lg sm:text-2xl capitalize font-serif font-semibold px-5 sm:px-10">
            You should always know where you are going
          </h2>
        </div> */}
      </div>
    </Layout>
  );
};
export default WishList;
