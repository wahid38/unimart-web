import React, { useState, useEffect, useContext } from "react";
import { useRecoilState } from "recoil";
import axios from "axios";
import { locationModal } from "src/recoil/locationHandler";
import { campaignProduct } from "src/recoil/productHandler";

//internal import
import Layout from "@layout/Layout";
import useFilter from "@hooks/useFilter";
import Card from "@component/cta-card/Card";
import OfferCard from "@component/offer/OfferCard";
import StickyCart from "@component/cart/StickyCart";
import Campaign from "@component/coupon/Campaign";
import ProductCard from "@component/product/ProductCard";
import MainCarousel from "@component/carousel/MainCarousel";
import FeatureCategory from "@component/category/FeatureCategory";
import ProductByCayegory from "@component/product/ProductByCayegory";
import LandingPageModal from "@component/modal/LandingPageModal";
import FeatureCard from "@component/feature-card/FeatureCard";
import FooterTop from "@layout/footer/FooterTop";
import { UserContext } from "@context/UserContext";

import Link from "next/link";
import { useRouter } from "next/router";
import {
  allCartItems,
  totalCartAmount,
  totalItems,
} from "src/recoil/cartHandler";
import { axiosGet } from "src/comon-func/AxiosGet";

const Home = () => {
  // const {
  //   state: { storeId },
  // } = useContext(UserContext);
  //console.log(storeId);
  // Stores
  const [stores, setStores] = useState([]);

  // Product By Store
  const [productsByStore, setProductsByStore] = useState([]);
  // Cart
  const [carts, setCarts] = useRecoilState(allCartItems);
  const [cartsItems, setCartsItems] = useRecoilState(totalItems);
  const [cartsAmount, setCartsAmount] = useRecoilState(totalCartAmount);

  useEffect(() => {
    const crt = JSON.parse(localStorage.getItem("uni-cart") || "{}");
    const crtItems = JSON.parse(localStorage.getItem("uni-cart-items") || 0);
    const crtAmount = JSON.parse(localStorage.getItem("uni-cart-amount") || 0);
    setCarts(crt);
    setCartsItems(crtItems);
    setCartsAmount(crtAmount);
  }, []);

  // Page On Load
  useEffect(() => {
    axiosGet("/Products/getproductsbycategories/7", setProductsByStore);
    axiosGet("/Stores/getlocations", setStores);
  }, []);
  return (
    <>
      {stores.length > 0 && <LandingPageModal stores={stores} />}
      <Layout>
        <div className="min-h-screen">
          <StickyCart />
          <div className="bg-white">
            <div className="mx-auto py-5 max-w-screen-2xl px-3 sm:px-10">
              <div className="lg:flex w-full">
                <div className="flex-shrink-0 xl:pr-1 lg:block w-full lg:w-3/5">
                  {/*  */}
                  <MainCarousel />
                  {/*  */}
                </div>
                <div className="w-full block mt-4 lg:mt-0 lg:ml-4 lg:flex">
                  {/*  */}
                  <OfferCard />
                  {/*  */}
                </div>
              </div>
            </div>

            {/* <div className="hidden relative lg:block mx-auto max-w-screen-2xl px-3 sm:px-10">
              <FeatureCard />
            </div> */}
          </div>

          {/* feature category's */}
          <div className="bg-white lg:py-5 pb-5 pt-0">
            <div className="mx-auto max-w-screen-2xl px-3 sm:px-10">
              <div className="mb-5 flex justify-center">
                <div className="text-center w-full lg:w-2/5">
                  <h2 className="text-xl lg:text-2xl mb-2 font-serif font-semibold">
                    Shop By Concern
                  </h2>
                  {/* <p className="text-base font-sans text-gray-600 leading-6">
                    Choose your necessary products from this feature categories.
                  </p> */}
                </div>
              </div>
              {/*  */}
              <FeatureCategory />
              {/*  */}
            </div>
          </div>

          {/* discounted products */}
          {/* <div className="bg-gray-50 lg:py-5 py-5 mx-auto max-w-screen-2xl px-3 sm:px-10">
            <div className="mb-5 flex justify-center">
              <div className="text-center w-full lg:w-2/5">
                <h2 className="text-xl lg:text-2xl mb-2 font-serif font-semibold">
                  Latest Discounted Products
                </h2>
                <p className="text-base font-sans text-gray-600 leading-6">
                  See Our latest discounted products below. Choose your daily
                  needs from here and get a special discount with free shipping.
                </p>
              </div>
            </div>
            <div className="flex">
              <div className="w-full">
                <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-5 xl:grid-cols-5 2xl:grid-cols-6 gap-2 md:gap-3 lg:gap-3">
                  {productData?.slice(0, 10).map((product, i) => (
                    <ProductCard key={i + 1} product={product} />
                  ))}
                </div>
              </div>
            </div>
            <div className='sm:text-right' >
            <Link href="/">
              <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-5  py-3 hover:text-white hover:bg-green-600 h-9 mt-3 text-sm lg:text-sm w-full sm:w-auto">
                Show More...
              </a>
            </Link>
            </div>
          </div> */}
          {/*  */}
          {/* <Campaign /> */}
          {/*  */}
          {/* promotional banner card */}
          <div className="block mt-2">
            <div className="mx-auto max-w-screen-2xl px-3 sm:px-10">
              <div className="grid gap-3 grid-cols-1 2xl:gap-6 xl:grid-cols-3 lg:grid-cols-3 md:grid-cols-2">
                <Card />
              </div>
            </div>
          </div>

          {/* popular products */}
          {/* <div className="bg-gray-50 lg:py-5 py-5 mx-auto max-w-screen-2xl px-3 sm:px-10">
            <div className="mb-5 flex justify-center">
              <div className="text-center w-full lg:w-2/5">
                <h2 className="text-xl lg:text-2xl mb-2 font-serif font-semibold">
                  Popular Products for Daily Shopping
                </h2>
                <p className="text-base font-sans text-gray-600 leading-6">
                  See all our popular products in this week. You can choose your
                  daily needs products from this list and get some special offer
                  with free shipping.
                </p>
              </div>
            </div>
            <div className="flex">
              <div className="w-full">
                <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-5 xl:grid-cols-5 2xl:grid-cols-6 gap-2 md:gap-3 lg:gap-3">
                  {products?.slice(0, 10).map((product, i) => (
                    <ProductCard key={i + 1} product={product} />
                  ))}
                </div>
              </div>
            </div>
            <div className='sm:text-right' >
            <Link href="/">
              <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-5  py-3 hover:text-white hover:bg-green-600 h-9 mt-3 text-sm lg:text-sm w-full sm:w-auto">
                Show More...
              </a>
            </Link>
            </div>
          </div> */}

          {/* Show product by category */}
          {/*  */}
          <ProductByCayegory productData={productsByStore} />
          {/*  */}
          {/* <div className="bg-gray-50 lg:py-1 py-2 mx-auto max-w-screen-2xl px-3 lg:pt-8 sm:px-10">
            <div className="py-5 flex justify-between items-center">
              <div className="lg:w-2/5">
                <h2 className="text-lg sm:text-xl capitalize lg:text-2xl  font-serif font-semibold">
                  Baby Care
                </h2>
              </div>
              <div>
                <Link href="/search?category=baby-food">
                  <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-2  py-2 md:px-5 md:py-2 hover:text-white hover:bg-green-600  text-xs lg:text-sm ">
                    Show More...
                  </a>
                </Link>
              </div>
            </div>
            <div className="flex">
              <div className="w-full">
                <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-6 xl:grid-cols-6 2xl:grid-cols-6 gap-2 md:gap-3 lg:gap-3">
                  {babyCare?.slice(0, 6).map((product, i) => (
                    <ProductCard key={i + 1} product={product} />
                  ))}
                </div>
              </div>
            </div>
          </div> */}
          {/*  */}

          {/* <div className="bg-gray-50 lg:py-1 py-2 mx-auto max-w-screen-2xl px-3 sm:px-10">
            <div className="py-5 flex justify-between items-center">
              <div className="lg:w-2/5">
                <h2 className="text-lg sm:text-xl capitalize lg:text-2xl  font-serif font-semibold">
                  Health and beauty
                </h2>
              </div>
              <div>
                <Link href="/search?category=bath">
                  <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-2  py-2 md:px-5 md:py-2 hover:text-white hover:bg-green-600  text-xs lg:text-sm ">
                    Show More...
                  </a>
                </Link>
              </div>
            </div>
            <div className="flex">
              <div className="w-full">
                <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-6 xl:grid-cols-6 2xl:grid-cols-6 gap-2 md:gap-3 lg:gap-3">
                  {healthBeauty?.slice(0, 6).map((product, i) => (
                    <ProductCard key={i + 1} product={product} />
                  ))}
                </div>
              </div>
            </div>
          </div> */}

          {/* Top Footer */}
          {/* <FooterTop /> */}
        </div>
      </Layout>
    </>
  );
};

export default Home;
