import Layout from "@layout/Layout";
import Cookies from "js-cookie";
import PageHeader from "@component/header/PageHeader";
import React, { useEffect, useState, useContext } from "react";
import useFilter from "@hooks/useFilter";
import { products } from "@utils/products";
import ProductCard from "@component/product/ProductCard";
import { isAdd } from "src/recoil/wishlistHandler";
import { useRecoilValue, waitForAll } from "recoil";
import useAsync from "@hooks/useAsync";

import { useRouter } from "next/router";
import { UserContext } from "@context/UserContext";
import { axiosGet, axiosGetOrders } from "src/comon-func/AxiosGet";

const WishList = () => {
  const [wishlist, setWishlist] = useState([]);
  const router = useRouter();

  const token = Cookies.get("user-auth");
  console.log(token);

  useEffect(() => {
    axiosGetOrders("/Customer/getwishlist", setWishlist, token);
    //console.log(wishlist);
  }, []);
  console.log(wishlist);

  const productData = useRecoilValue(isAdd);

  return (
    <Layout title="Wish List" description="This is wish list page">
      <PageHeader title="Your Favorite" />
      {/*  */}
      <div className="bg-gray-50 lg:py-5 pt-5 mx-auto max-w-screen-2xl px-3 sm:px-10">
        <div className="flex">
          <div className="w-full">
            <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-6 xl:grid-cols-6 2xl:grid-cols-6 gap-2 md:gap-3 lg:gap-3">
              {productData?.slice(0, 12).map((product) => (
                <ProductCard key={product.id} product={product} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default WishList;

// export const getStaticProps = async () => {
//   const item = await WishlistService.getWishlistItem();
//   return {
//     props: {
//       productList: item,
//     },
//   };
// };
