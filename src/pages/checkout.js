import React, { useEffect, useRef, useState } from "react";
import dynamic from "next/dynamic";
import Link from "next/link";
import {
  IoReturnUpBackOutline,
  IoArrowForward,
  IoBagHandle,
  IoWalletSharp,
} from "react-icons/io5";
import { ImCreditCard } from "react-icons/im";

//internal import
import Layout from "@layout/Layout";
import CartItem from "@component/cart/CartItem";
import UserAddress from "@component/checkout/UserAddress";
import AddressModal from "@component/modal/AddressModal";
import SubscriptionModal from "@component/modal/SubscriptionModal";
import InputPayment from "@component/form/InputPayment";
import useCheckoutSubmit from "@hooks/useCheckoutSubmit";
import { useRecoilState } from "recoil";
import { subscriptionModal } from "src/recoil/subscriptionHandler";
import { axiosGetExtra } from "src/comon-func/AxiosGet";
import {
  allCartItems,
  totalCartAmount,
  totalItems,
} from "src/recoil/cartHandler";
import axios from "axios";
import Cookies from "js-cookie";
import { notifyError, notifySuccess } from "@utils/toast";
import { placeOrder } from "src/comon-func/OrderHandler";
import {
  isRecurring,
  recurringContinuty,
  recurringInterval,
  recurringUnitTime,
} from "src/recoil/recurringOrder";

const Checkout = () => {
  const [isOpen, setIsOpen] = useRecoilState(subscriptionModal);

  const handleChecked = (event) => {
    console.log(event.target.checked);
    if (event.target.checked) {
      //toggleCartDrawer();
      setIsOpen(!isOpen);
    }
  };
  // JWT
  const token = Cookies.get("user-auth");

  //
  const [modalOpen, setModalOpen] = useState(false);
  const [paymentType, setPaymentType] = useState(null);
  const free = 0;

  // Recurring
  const [timeIntarval, setTimeIntarval] = useRecoilState(recurringInterval);
  const [monthOrWeek, setMonthOrWeek] = useRecoilState(recurringUnitTime);
  const [recurringCont, setRecurringCont] = useRecoilState(recurringContinuty);
  const [recurring, setRecurring] = useRecoilState(isRecurring);

  // Customer Address
  const [customerAddress, setCustomerAddress] = useState([]);
  const [selectedAddress, setSelectedAddress] = useState({});
  const [isSelect, setIsSelect] = useState(null);

  // Cart
  const [carts, setCarts] = useRecoilState(allCartItems);
  const [cartsItems, setCartsItems] = useRecoilState(totalItems);
  const [cartsAmount, setCartsAmount] = useRecoilState(totalCartAmount);

  const { register } = useCheckoutSubmit();

  // Coupon Info
  const couponRef = useRef();
  const [coupon, setCoupon] = useState({});
  const [appliedCouponAmount, setAppliedCouponAmount] = useState(0);
  const [appliedCouponName, setAppliedCouponName] = useState("");
  const [afterDiscount, setAfterDiscount] = useState(0);
  const callCoupon = async (e) => {
    e.preventDefault();
    try {
      const result = await axios({
        method: "GET",
        url: `/GeneralVouchers/check/${couponRef.current.value}`,
      });
      if (Object.keys(result.data).length === 0) {
        notifyError("Coupon Doesn't Exist!");
        return;
      }
      if (cartsAmount < result.data.minCartAmmount) {
        notifyError(
          `Minimum Purchase Amount is ${result.data.minCartAmmount} Taka!`
        );
        return;
      }
      setCoupon(result.data);
      checkCoupon(result.data);
    } catch (error) {
      console.error(error);
      notifyError("Something Went Wrong!");
    }
  };

  const checkCoupon = (data) => {
    if (data.discountType === "percent") {
      const discAmount = (cartsAmount * data.percentageOrAmount) / 100;
      if (discAmount > data.uptoDiscountAmount) {
        setAppliedCouponAmount(data.uptoDiscountAmount);
        setAfterDiscount(cartsAmount - data.uptoDiscountAmount);
        return;
      }
      setAppliedCouponAmount(discAmount);
      setAfterDiscount(cartsAmount - discAmount);
      setAppliedCouponName(data.voucherCode);
      return;
    }

    setAppliedCouponAmount(data.percentageOrAmount);
    setAfterDiscount(cartsAmount - data.percentageOrAmount);
    setAppliedCouponName(data.voucherCode);
  };

  // Place Order
  const addOrder = () => {
    placeOrder(
      isSelect,
      paymentType,
      carts,
      selectedAddress,
      token,
      timeIntarval,
      cartsAmount,
      selectedAddress?.zone?.[0]?.regulaPrice,
      appliedCouponName,
      afterDiscount,
      monthOrWeek,
      appliedCouponAmount,
      setCarts,
      setCartsItems,
      setCartsAmount,
      setCoupon,
      setAppliedCouponAmount,
      setAppliedCouponName,
      setAfterDiscount,
      couponRef,
      setTimeIntarval,
      setMonthOrWeek,
      setRecurringCont,
      setRecurring,
      recurringCont,
      setIsSelect,
      setSelectedAddress
    );
  };

  // Page On Load
  useEffect(() => {
    axiosGetExtra(
      "/DeliveryAddress/getaddressbycustomer",
      setCustomerAddress,
      token
    );
  }, []);

  useEffect(() => {
    const crt = JSON.parse(localStorage.getItem("uni-cart") || "[]");
    const crtItems = JSON.parse(localStorage.getItem("uni-cart-items") || "0");
    const crtAmount = JSON.parse(
      localStorage.getItem("uni-cart-amount") || "0"
    );
    setCarts(crt);
    setCartsItems(crtItems);
    setCartsAmount(crtAmount);
  }, []);

  useEffect(() => {
    if (Object.keys(coupon).length !== 0) {
      checkCoupon(coupon);
    }
  }, [cartsAmount]);
  return (
    <>
      <SubscriptionModal modalOpen={isOpen} setModalOpen={setIsOpen} />
      <AddressModal
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        loadAddress={() =>
          axiosGetExtra(
            "/DeliveryAddress/getaddressbycustomer",
            setCustomerAddress,
            token
          )
        }
      />
      <Layout title="Checkout" description="this is checkout page">
        <div className="mx-auto max-w-screen-2xl px-3 sm:px-10">
          <div className="py-8 lg:py-8 px-0 2xl:max-w-screen-2xl w-full xl:max-w-screen-2xl flex flex-col md:flex-row lg:flex-row  md:items-center">
            <div className="md:w-full lg:w-3/5 flex h-full flex-col order-2 sm:order-1 lg:order-1">
              <div className="mt-5 md:mt-0 md:col-span-2">
                {customerAddress.length > 0 && (
                  <div className="grid grid-cols-1 lg:grid-cols-3 gap-3 xl:gap-5 justify-center items-center pb-6">
                    {customerAddress.slice(0, 3).map((item, index) => (
                      <UserAddress
                        key={index}
                        item={item}
                        index={index}
                        isSelect={isSelect}
                        setIsSelect={setIsSelect}
                        setSelectedAddress={setSelectedAddress}
                      />
                    ))}
                  </div>
                )}
                <button
                  onClick={() => setModalOpen(!modalOpen)}
                  className="border-solid bg-green-500 hover:bg-green-600 border border-green-500 transition-all text-center w-full lg:w-3/6 rounded py-3 text-sm font-serif font-medium text-white mx-auto flex justify-center items-center mb-8"
                >
                  Add New Address
                </button>
                <form>
                  <input
                    type="checkbox"
                    //checked
                    name="check"
                    className="border border-solid border-green-500 rounded outline-0"
                    onChange={(event) => handleChecked(event)}
                  />{" "}
                  I want this as recurring order
                  <div className="form-group mt-6">
                    <h2 className="font-semibold font-serif text-base text-gray-700 pb-3">
                      Payment Details
                    </h2>
                    <div className="grid grid-cols-6 gap-6">
                      <div
                        onClick={() => setPaymentType(1)}
                        className="col-span-6 sm:col-span-3"
                      >
                        <InputPayment
                          // setShowCard={setShowCard}
                          register={register}
                          name="Cash On Delivery"
                          value="COD"
                          Icon={IoWalletSharp}
                        />
                      </div>

                      <div
                        onClick={() => setPaymentType(2)}
                        className="col-span-6 sm:col-span-3"
                      >
                        <InputPayment
                          // setShowCard={setShowCard}
                          register={register}
                          name="Card / Mobile Banking"
                          value="Card"
                          Icon={ImCreditCard}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-6 gap-4 lg:gap-6 mt-10">
                    <div className="col-span-6 sm:col-span-3">
                      <Link href="/">
                        <a className="bg-indigo-50 border border-indigo-100 rounded py-3 text-center text-sm font-medium text-gray-700 hover:text-gray-800 hover:border-gray-300 transition-all flex justify-center font-serif w-full">
                          <span className="text-xl mr-2">
                            <IoReturnUpBackOutline />
                          </span>
                          Continue Shopping
                        </a>
                      </Link>
                    </div>
                    <div
                      onClick={() => addOrder()}
                      className="col-span-6 sm:col-span-3"
                    >
                      <button
                        type="button"
                        className="bg-green-500 hover:bg-green-600 border border-green-500 transition-all rounded py-3 text-center text-sm font-serif font-medium text-white flex justify-center w-full"
                      >
                        Confirm Order{" "}
                        <span className="text-xl ml-2">
                          {" "}
                          <IoArrowForward />
                        </span>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

            <div className="md:w-full lg:w-2/5 lg:ml-10 xl:ml-14 md:ml-6 flex flex-col h-full md:sticky lg:sticky top-28 md:order-2 lg:order-2">
              <div className="border p-5 lg:px-8 lg:py-8 rounded-lg bg-white order-1 sm:order-2">
                <h2 className="font-semibold font-serif text-lg pb-4">
                  Order Summary
                </h2>

                <div className="overflow-y-scroll flex-grow scrollbar-hide w-full max-h-64 bg-gray-50 block">
                  {carts?.orderProductsDTOs?.map((item) => (
                    <CartItem key={item.productId} item={item} />
                  ))}
                  {carts?.campaignDTOs?.map((item) => (
                    <CartItem key={item.productId} item={item} />
                  ))}

                  {!cartsItems && (
                    <div className="text-center py-10">
                      <span className="flex justify-center my-auto text-green-500 font-semibold text-5xl">
                        <IoBagHandle />
                      </span>
                      <h2 className="font-medium font-serif text-sm pt-2 text-gray-600">
                        No Item Added Yet!
                      </h2>
                    </div>
                  )}
                </div>
                {/* Apply Coupon Section */}
                <div className="flex items-center mt-4 py-4 lg:py-4 text-sm w-full font-semibold text-heading last:border-b-0 last:text-base last:pb-0">
                  <form className="w-full">
                    {!appliedCouponName ? (
                      <div className="flex flex-col sm:flex-row items-start justify-end">
                        <input
                          ref={couponRef}
                          type="text"
                          placeholder="Input your coupon code"
                          className="form-input py-2 px-3 md:px-4 w-full appearance-none transition ease-in-out border text-input text-sm rounded-md h-12 duration-200 bg-white border-gray-200 focus:ring-0 focus:outline-none focus:border-green-500 placeholder-gray-500 placeholder-opacity-75"
                        />
                        <button
                          onClick={callCoupon}
                          className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border border-gray-200 rounded-md placeholder-white focus-visible:outline-none focus:outline-none px-5 md:px-6 lg:px-8 py-3 md:py-3.5 lg:py-3 mt-3 sm:mt-0 sm:ml-3 md:mt-0 md:ml-3 lg:mt-0 lg:ml-3 hover:text-white hover:bg-green-500 h-12 text-sm lg:text-base w-full sm:w-auto"
                        >
                          Apply
                        </button>
                      </div>
                    ) : (
                      <span className="bg-green-50 px-4 py-3 leading-tight w-full rounded-md flex justify-between">
                        {" "}
                        <p className="text-green-600">Coupon Applied </p>{" "}
                        <span className="text-red-500 text-right">
                          {appliedCouponName}
                        </span>
                      </span>
                    )}
                  </form>
                </div>
                <div className="flex items-center py-2 text-sm w-full font-semibold text-gray-500 last:border-b-0 last:text-base last:pb-0">
                  Subtotal
                  <span className="ml-auto flex-shrink-0 text-gray-800 font-bold">
                    &#2547; {cartsAmount.toFixed(2)}
                  </span>
                </div>
                <div className="flex items-center py-2 text-sm w-full font-semibold text-gray-500 last:border-b-0 last:text-base last:pb-0">
                  Shipping Cost
                  <span className="ml-auto flex-shrink-0 text-gray-800 font-bold">
                    &#2547;{" "}
                    {selectedAddress?.zone?.[0]?.regulaPrice.toFixed(2) ||
                      free.toFixed(2)}
                  </span>
                </div>
                <div className="flex items-center py-2 text-sm w-full font-semibold text-gray-500 last:border-b-0 last:text-base last:pb-0">
                  Discount
                  <span className="ml-auto flex-shrink-0 font-bold text-orange-400">
                    &#2547;{" "}
                    {appliedCouponAmount
                      ? appliedCouponAmount.toFixed(2)
                      : free.toFixed(2)}
                  </span>
                </div>
                <div className="border-t mt-4">
                  <div className="flex items-center font-bold font-serif justify-between pt-5 text-sm uppercase">
                    Total cost
                    <span className="font-serif font-extrabold text-lg">
                      {" "}
                      &#2547;{" "}
                      {appliedCouponAmount
                        ? Math.round(
                            afterDiscount +
                              (selectedAddress?.zone?.[0]?.regulaPrice || 0)
                          )
                        : Math.round(
                            cartsAmount +
                              (selectedAddress?.zone?.[0]?.regulaPrice || 0)
                          )}
                      .00
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default dynamic(() => Promise.resolve(Checkout), { ssr: false });
