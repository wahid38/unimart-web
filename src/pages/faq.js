import React from "react";
import Image from "next/image";
import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";

//internal import
import Layout from "@layout/Layout";
import PageHeader from "@component/header/PageHeader";

const Faq = () => {
  return (
    <Layout title="FAQ" description="This is faq page">
      <PageHeader title="Frequently Asked Questions" />
      <div className="bg-white">
        <div className="max-w-screen-2xl mx-auto px-3 sm:px-10 py-10 lg:py-12">
          <div className="grid gap-4 lg:mb-8 items-center md:grid-cols-2 xl:grid-cols-2">
            <div className="pr-16">
              <Image width={720} height={550} src="/faq.svg" alt="logo" />
            </div>
            <div className="">
              <Disclosure>
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                      <span>How does the site work?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      You can browse the site or use our search engine to find
                      your desired products. You can add them to your shopping
                      cart and click on Place Order. As you are entering your
                      details including address, phone number Farmroots or its
                      dedicated logistics partner will deliver the package at
                      your doorstep.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>

              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>How much do deliveries cost?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      We calculate your order items shipping charge based on
                      products weights as follows:
                      <br /> Inside Dhaka City Corporation: 59/-
                      <br /> Outside Dhaka City Corporation: Up to 1 kg 99/-
                      <br />
                      10/- Per 1 gm to 1000 gm above 1kg for all products.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>

              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>How can I contact you?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      You can always call on +8809604656565 or mail us at
                      support@farmroots.com.bd.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>What are your delivery time?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      Inside Dhaka: We deliver from 10 am to 6 pm every day.
                      <br />
                      Outside Dhaka: Our logistics partner deliver within 3-7
                      days.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>How do I know when my order is here?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      A Farmroots representative or our logistics partner shall
                      call you when they start for your location and again they
                      will call you as soon as they are at your delivery
                      address.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>How do I pay?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      We accept Cash on delivery, Credit/Debit card, online
                      payment & most prominent mobile wallet services. Don’t
                      worry, our Farmroots representatives should always carry
                      enough change.
                      <br />
                      BKASH merchant account number 01322840740.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure>
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                      <span>Do you serve my area?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      Yes, We serve all over Bangladesh. So, you can order from
                      right where you are now.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>

              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>
                        I can’t find the product I am looking for. What should I
                        do?
                      </span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      Whenever you search any product on Farmroots site and if
                      it is not found then click on the option “Suggest a
                      Product”, please fill up the full requirement and we will
                      get back to you. You can also call on +8809604656565 or
                      mail us at support@farmroots.com.bd about your required
                      products.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>My order is wrong. What should I do?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      Please immediately call on +8809604656565 and let us know
                      the problem.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>What happens during a hartals/strikes?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      We work during hartals/strikes until it becomes life risky
                      and violent. In these events your order may get delayed to
                      deliver. But we will keep you updated.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>
                        Why should we buy from you when I have a store nearby?
                      </span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      We always focus only on your family Health. We only
                      display & sell those products which ensures 100% purity.
                      To have this surety of pure products our customers depends
                      on us over the regular store or sites.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>What is your policy on refunds?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      Please check our RETURN and REFUND POLICY.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>Can I order over the phone?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      Absolutely. Our hotline is +8809604656565.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>How are you sourcing your products?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      We sell our own manufactured products & only reputed
                      international brand’s products. We work with only fully
                      compliant importers & distributors to ensure your food
                      quality at its best level.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
              <Disclosure as="div" className="mt-2">
                {({ open }) => (
                  <>
                    <Disclosure.Button className="flex justify-between w-full px-4 py-3 text-base font-medium text-left text-gray-600 focus:text-green-500 bg-gray-50 hover:bg-green-50 rounded-lg focus:outline-none">
                      <span>Should I tip the delivery representative?</span>
                      <ChevronUpIcon
                        className={`${
                          open ? "transform rotate-180 text-green-500" : ""
                        } w-5 h-5 text-gray-500`}
                      />
                    </Disclosure.Button>
                    <Disclosure.Panel className="px-4 pt-3 pb-8 text-sm leading-7 text-gray-500">
                      Tips are not required. Our delivery team members never
                      accept tips from customers who are compared to the kins in
                      our business.
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Faq;
