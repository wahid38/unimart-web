import "@styles/custom.css";
import "react-multi-carousel/lib/styles.css";
import { CartProvider } from "react-use-cart";
import { RecoilRoot } from "recoil";

//internal import
import { UserProvider } from "@context/UserContext";
import { SidebarProvider } from "@context/SidebarContext";
import DefaultSeo from "@component/common/DefaultSeo";
import axios from "axios";

axios.defaults.baseURL = "https://test.bespokeit.io/api";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <RecoilRoot>
        <UserProvider>
          <SidebarProvider>
            <CartProvider>
              <DefaultSeo />
              <Component {...pageProps} />
            </CartProvider>
          </SidebarProvider>
        </UserProvider>
      </RecoilRoot>
    </>
  );
}

export default MyApp;
