import React from "react";

//internal import
import Layout from "@layout/Layout";
import PageHeader from "@component/header/PageHeader";

const PrivacyPolicy = () => {
  return (
    <Layout title="Privacy Policy" description="This is privacy policy page">
      <PageHeader title="Privacy Policy" />
      <div className="bg-white">
        <div className="max-w-screen-2xl mx-auto lg:py-10 py-10 px-4 sm:px-10">
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              PRIVACY POLICY
            </h2>
            <div className="font-sans leading-7">
              <p>
                At https://www.unimart.online, accessible from
                unimart.bangladesh@gmail.com, one of our main priorities is the
                privacy of our visitors. This Privacy Policy document contains
                types of information that is collected and recorded by
                https://www.unimart.online and how we use it.
              </p>
              <p>
                If you have additional questions or require more information
                about our Privacy Policy, do not hesitate to contact us.
              </p>
              <p>
                This Privacy Policy applies only to our online activities and is
                valid for visitors to our website with regards to the
                information that they shared and/or collect in
                https://www.unimart.online. This policy is not applicable to any
                information collected offline or via channels other than this
                website.
              </p>
            </div>
          </div>
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              CONSENT
            </h2>
            <div className="font-sans leading-7">
              <p>
                By using our website, you hereby consent to our Privacy Policy
                and agree to its terms.
              </p>
            </div>
          </div>
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              INFORMATION WE COLLECT
            </h2>
            <div className="font-sans leading-7">
              <p>
                The personal information that you are asked to provide, and the
                reasons why you are asked to provide it, will be made clear to
                you at the point we ask you to provide your personal
                information.
              </p>
              <p>
                If you contact us directly, we may receive additional
                information about you such as your name, email address, phone
                number, the contents of the message and/or attachments you may
                send us, and any other information you may choose to provide.
              </p>
              <p>
                When you register for an Account, we may ask for your contact
                information, including items such as name, company name,
                address, email address, and telephone number.
              </p>
            </div>
          </div>
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              HOW WE USE YOUR INFORMATION
            </h2>
            <div className="font-sans leading-7">
              <p>
                We use the information we collect in various ways, including to:
                · Provide, operate, and maintain our website · Improve,
                personalize, and expand our website · Understand and analyze how
                you use our website · Develop new products, services, features,
                and functionality · Communicate with you, either directly or
                through one of our partners, including for customer service, to
                provide you with updates and other information relating to the
                website, and for marketing and promotional purposes · Send you
                emails · Find and prevent fraud
              </p>
              <p>
                LOG FILES https://www.unimart.online follows a standard
                procedure of using log files. These files log visitors when they
                visit websites. All hosting companies do this and a part of
                hosting services' analytics. The information collected by log
                files includes internet protocol (IP) addresses, browser type,
                Internet Service Provider (ISP), date and time stamp,
                referring/exit pages, and possibly the number of clicks. These
                are not linked to any information that is personally
                identifiable. The purpose of the information is for analyzing
                trends, administering the site, tracking users' movement on the
                website, and gathering demographic information. Our Privacy
                Policy was created with the help of the Privacy Policy Generator
                and the Disclaimer Generator.
              </p>
              <p>
                COOKIES AND WEB BEACONS Like any other website,
                https://www.unimart.online uses 'cookies'. These cookies are
                used to store information including visitors' preferences, and
                the pages on the website that the visitor accessed or visited.
                The information is used to optimize the users' experience by
                customizing our web page content based on visitors' browser type
                and/or other information.
              </p>
              <p>
                For more general information on cookies, please read "What Are
                Cookies".
              </p>
            </div>
          </div>
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              ADVERTISING PARTNERS PRIVACY POLICIES
            </h2>
            <div className="font-sans leading-7">
              <p>
                You may consult this list to find the Privacy Policy for each of
                the advertising partners of https://www.unimart.online.
              </p>
              <p>
                Third-party ad servers or ad networks use technologies like
                cookies, JavaScript, or Web Beacons that are used in their
                respective advertisements and links that appear on
                https://www.unimart.online, which are sent directly to users'
                browser. They automatically receive your IP address when this
                occurs. These technologies are used to measure the effectiveness
                of their advertising campaigns and/or to personalize the
                advertising content that you see on websites that you visit.
              </p>
              <p>
                Note that https://www.unimart.online has no access to or control
                over these cookies that are used by third-party advertisers.
              </p>
              <p>
                THIRD-PARTY PRIVACY POLICIES https://www.unimart.online's
                Privacy Policy does not apply to other advertisers or websites.
                Thus, we are advising you to consult the respective Privacy
                Policies of these third-party ad servers for more detailed
                information. It may include their practices and instructions
                about how to opt-out of certain options.
              </p>
              <p>
                You can choose to disable cookies through your individual
                browser options. To know more detailed information about cookie
                management with specific web browsers, it can be found at the
                browsers' respective websites.
              </p>
            </div>
          </div>
          <div className="mb-8 lg:mb-12 last:mb-0">
            <h2 className="text-xl xl:text-2xl xl:leading-7 font-semibold font-serif mb-2 lg:mb-4">
              CHILDREN'S INFORMATION
            </h2>
            <div className="font-sans leading-7">
              <p>
                Another part of our priority is adding protection for children
                while using the internet. We encourage parents and guardians to
                observe, participate in, and/or monitor and guide their online
                activity. https://www.unimart.online does not knowingly collect
                any Personal Identifiable Information from children under the
                age of 13. If you think that your child provided this kind of
                information on our website, we strongly encourage you to contact
                us immediately and we will do our best efforts to promptly
                remove such information from our records.
              </p>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default PrivacyPolicy;
