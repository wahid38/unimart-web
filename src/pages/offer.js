//internal import
import Layout from "@layout/Layout";
import PageHeader from "@component/header/PageHeader";
import Campaign from "@component/coupon/Campaign";
import { useRecoilState } from "recoil";
import { locationModal } from "src/recoil/locationHandler";
import LandingPageModal from "@component/modal/LandingPageModal";
import {
  allCartItems,
  totalCartAmount,
  totalItems,
} from "src/recoil/cartHandler";
import { useEffect } from "react";

const Offer = () => {
  // Cart
  const [carts, setCarts] = useRecoilState(allCartItems);
  const [cartsItems, setCartsItems] = useRecoilState(totalItems);
  const [cartsAmount, setCartsAmount] = useRecoilState(totalCartAmount);

  useEffect(() => {
    const crt = JSON.parse(localStorage.getItem("uni-cart") || "{}");
    const crtItems = JSON.parse(localStorage.getItem("uni-cart-items") || 0);
    const crtAmount = JSON.parse(localStorage.getItem("uni-cart-amount") || 0);
    setCarts(crt);
    setCartsItems(crtItems);
    setCartsAmount(crtAmount);
  }, []);
  return (
    <>
      <Layout title="Offer" description="this is discount page">
        <PageHeader title="Mega Offer" />
        <div className="mx-auto max-w-screen-2xl py-0 lg:pt-0 sm:px-0">
          {/* <div className="grid gap-6 grid-cols-1 xl:grid-cols-2"> */}
          <Campaign />
          {/* <Coupon /> */}
          {/* </div> */}
        </div>
      </Layout>
    </>
  );
};

export default Offer;
