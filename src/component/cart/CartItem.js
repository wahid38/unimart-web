import { useContext } from "react";
import Link from "next/link";
import Image from "next/image";
import { useCart } from "react-use-cart";
import { FiPlus, FiMinus, FiTrash2 } from "react-icons/fi";

//internal import
import { SidebarContext } from "@context/SidebarContext";
import { useRecoilState } from "recoil";
import {
  allCartItems,
  totalCartAmount,
  totalItems,
} from "src/recoil/cartHandler";

const CartItem = ({ item }) => {
  const { closeCartDrawer } = useContext(SidebarContext);

  ///////////////////////
  ///////////////////////
  ///////////////////////
  ///////////////////////
  ///////////////////////
  const [carts, setCarts] = useRecoilState(allCartItems);
  const [cartsItems, setCartsItems] = useRecoilState(totalItems);
  const [cartsAmount, setCartsAmount] = useRecoilState(totalCartAmount);

  // Plus One
  const addToCartPlusOne = (product) => {
    let cart = JSON.parse(JSON.stringify(carts));
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items"));
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount"));

    if (product?.campaignId !== undefined) {
      const cartIndex = cart.campaignDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.campaignDTOs[cartIndex].quantity += 1;
    }
    if (product?.campaignId === undefined) {
      const cartIndex = cart.orderProductsDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.orderProductsDTOs[cartIndex].quantity += 1;
    }

    localStorage.setItem("uni-cart", JSON.stringify(cart));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem + 1));
    localStorage.setItem(
      "uni-cart-amount",
      JSON.stringify(cartAmount + product.productPrice)
    );

    setCarts(cart);
    setCartsItems((old) => old + 1);
    setCartsAmount((old) => old + product.productPrice);
  };

  // Minus One
  const addToCartMinusOne = (product) => {
    let cart = JSON.parse(JSON.stringify(carts));
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items"));
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount"));

    if (product?.campaignId !== undefined) {
      const cartIndex = cart.campaignDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.campaignDTOs[cartIndex].quantity -= 1;
      const cartQuantity = cart.campaignDTOs[cartIndex].quantity;
      if (cartQuantity < 1) {
        let campProdCart = cart.campaignDTOs;
        campProdCart = campProdCart.filter(
          (el) => el.productId !== product.productId
        );
        cart.campaignDTOs = campProdCart;
      }
    }
    if (product?.campaignId === undefined) {
      const cartIndex = cart.orderProductsDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.orderProductsDTOs[cartIndex].quantity -= 1;
      const cartQuantity = cart.orderProductsDTOs[cartIndex].quantity;
      if (cartQuantity < 1) {
        let orderProdCart = cart.campaignDTOs;
        orderProdCart = orderProdCart.filter(
          (el) => el.productId !== product.productId
        );
        cart.orderProductsDTOs = orderProdCart;
      }
    }

    localStorage.setItem("uni-cart", JSON.stringify(cart));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem - 1));
    localStorage.setItem(
      "uni-cart-amount",
      JSON.stringify(cartAmount - product.productPrice)
    );

    setCarts(cart);
    setCartsItems((old) => old - 1);
    setCartsAmount((old) => old - product.productPrice);
  };

  // Remove Product
  const removeProduct = (product) => {
    let cart = JSON.parse(JSON.stringify(carts));
    let cartItem = JSON.parse(localStorage.getItem("uni-cart-items"));
    let cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount"));

    if (product?.campaignId !== undefined) {
      let campCart = cart.campaignDTOs;
      campCart = campCart.filter((el) => el.productId !== product.productId);
      cart.campaignDTOs = campCart;
    }
    if (product?.campaignId === undefined) {
      let prodCart = cart.orderProductsDTOs;
      prodCart = prodCart.filter((el) => el.productId !== product.productId);
      cart.orderProductsDTOs = prodCart;
    }

    cartItem = cartItem - product.quantity;
    cartAmount = cartAmount - product.quantity * product.productPrice;

    localStorage.setItem("uni-cart", JSON.stringify(cart));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem));
    localStorage.setItem("uni-cart-amount", JSON.stringify(cartAmount));

    setCarts(cart);
    setCartsItems(cartItem);
    setCartsAmount(cartAmount);
  };

  return (
    <div className="group w-full h-auto flex justify-start items-center bg-white py-3 px-4 border-b hover:bg-gray-50 transition-all border-gray-100 relative last:border-b-0">
      <div className="relative flex rounded-full border border-gray-100 shadow-sm overflow-hidden flex-shrink-0 cursor-pointer mr-4">
        <Image
          key={item.productId}
          src={`https://test.bespokeit.io/${item.image}`}
          width={50}
          height={50}
          alt={item.productName}
          objectFit="cover"
        />
      </div>
      <div className="flex flex-col w-full overflow-hidden">
        <Link href={`/product/${item.slug}`}>
          <a
            onClick={closeCartDrawer}
            className="truncate text-sm font-medium text-gray-700 text-heading line-clamp-1"
          >
            {item.productName}
          </a>
        </Link>
        <span className="text-xs text-gray-400 mb-1">
          Item Price &#2547; {item.productPrice}
        </span>
        <div className="flex items-center justify-between">
          <div className="font-bold text-sm md:text-base text-heading leading-5">
            <span>&#2547;{(item.productPrice * item.quantity).toFixed(2)}</span>
          </div>
          <div className="h-8 w-22 md:w-24 lg:w-24 flex flex-wrap items-center justify-evenly p-1 border border-gray-100 bg-white text-gray-600 rounded-md">
            <button onClick={() => addToCartMinusOne(item)}>
              <span className="text-dark text-base">
                <FiMinus />
              </span>
            </button>
            <p className="text-sm font-semibold text-dark px-1">
              {item.quantity}
            </p>
            <button onClick={() => addToCartPlusOne(item)}>
              <span className="text-dark text-base">
                <FiPlus />
              </span>
            </button>
          </div>
          <button
            onClick={() => removeProduct(item)}
            className="hover:text-red-600 text-red-400 text-lg cursor-pointer"
          >
            <FiTrash2 />
          </button>
        </div>
      </div>
    </div>
  );
};

export default CartItem;
