import Link from "next/link";
import { useRouter } from "next/router";
import React, { useContext, useState, useEffect } from "react";
import { useCart } from "react-use-cart";
import { IoBagCheckOutline, IoClose, IoBagHandle } from "react-icons/io5";

//internal import
import CartItem from "@component/cart/CartItem";
import LoginModal from "@component/modal/LoginModal";
import SubscriptionModal from "@component/modal/SubscriptionModal";
import { UserContext } from "@context/UserContext";
import { SidebarContext } from "@context/SidebarContext";
//RECOIL
import { useRecoilState } from "recoil";
// import { subscriptionModal } from "src/recoil/subscriptionHandler";
import {
  allCartItems,
  totalCartAmount,
  totalItems,
} from "src/recoil/cartHandler";
import Cookies from "js-cookie";

const Cart = () => {
  const [modalOpen, setModalOpen] = useState(false);
  // const [isOpen, setIsOpen] = useRecoilState(subscriptionModal);
  const router = useRouter();
  const { toggleCartDrawer, closeCartDrawer } = useContext(SidebarContext);

  const handleOpenLogin = () => {
    if (router.push("/?redirect=/checkout")) {
      toggleCartDrawer();
      setModalOpen(!modalOpen);
    }
  };

  // const handleChecked = (event) => {
  //   console.log(event.target.checked);
  //   if (event.target.checked) {
  //     toggleCartDrawer();
  //     setIsOpen(!isOpen);
  //   }
  // };

  ////////////////////////////
  ////////////////////////////
  ////////////////////////////
  ////////////////////////////
  ////////////////////////////
  ////////////////////////////

  // User Auth
  const userInfo = Cookies.get("user-auth");
  const [carts, setCarts] = useRecoilState(allCartItems);
  const [cartsItems, setCartsItems] = useRecoilState(totalItems);
  const [cartsAmount, setCartsAmount] = useRecoilState(totalCartAmount);

  const checkoutClass = (
    <button
      onClick={closeCartDrawer}
      className="w-full py-3 px-3 rounded-lg bg-green-500 hover:bg-green-600 flex items-center justify-between bg-heading text-sm sm:text-base text-white focus:outline-none transition duration-300"
    >
      <span className="align-middle font-medium font-serif">
        Proceed To Checkout
      </span>
      <span className="rounded-lg font-bold font-serif py-2 px-3 bg-white text-green-600">
        &#2547;{cartsAmount}
      </span>
    </button>
  );

  return (
    <>
      {/* <SubscriptionModal modalOpen={isOpen} setModalOpen={setIsOpen} /> */}
      {modalOpen && (
        <LoginModal modalOpen={modalOpen} setModalOpen={setModalOpen} />
      )}
      <div className="flex flex-col w-full h-full justify-between items-middle bg-white rounded cursor-pointer">
        <div className="w-full flex justify-between items-center relative px-5 py-4 border-b bg-indigo-50 border-gray-100">
          <h2 className="font-semibold font-serif text-lg m-0 text-heading flex items-center">
            <span className="text-xl mr-2 mb-1">
              <IoBagCheckOutline />
            </span>
            Shopping Cart
          </h2>
          <button
            onClick={closeCartDrawer}
            className="inline-flex text-base items-center justify-center text-gray-500 p-2 focus:outline-none transition-opacity hover:text-red-400"
          >
            <IoClose className="text-green-500" />
            <span className="font-sens text-sm text-gray-500 hover:text-red-400 ml-1">
              Close
            </span>
          </button>
        </div>
        <div className="overflow-y-scroll flex-grow scrollbar-hide w-full max-h-full">
          {!cartsItems && (
            <div className="flex flex-col h-full justify-center">
              <div className="flex flex-col items-center">
                <div className="flex justify-center items-center w-20 h-20 rounded-full bg-green-100">
                  <span className="text-green-600 text-4xl block">
                    <IoBagHandle />
                  </span>
                </div>
                <h3 className="font-serif font-semibold text-gray-700 text-lg pt-5">
                  Your cart is empty
                </h3>
                <p className="px-12 text-center text-sm text-gray-500 pt-2">
                  No items added in your cart. Please add product to your cart
                  list.
                </p>
              </div>
            </div>
          )}

          {carts.orderProductsDTOs?.map((item) => (
            <CartItem key={item.productId} item={item} />
          ))}
          {carts.campaignDTOs?.map((item) => (
            <CartItem key={item.productId} item={item} />
          ))}
        </div>
        <div className="mx-5 my-3">
          {/* <input
            type="checkbox"
            //checked
            name="check"
            className="border border-solid border-green-500 rounded outline-0"
            onChange={(event) => handleChecked(event)}
          />{" "}
          I want this as recurring order */}
          {carts.length <= 0 ? (
            checkoutClass
          ) : (
            <span>
              {!userInfo ? (
                <div onClick={handleOpenLogin}>{checkoutClass}</div>
              ) : (
                <Link href="/checkout">
                  <a>{checkoutClass}</a>
                </Link>
              )}
            </span>
          )}
        </div>
      </div>
    </>
  );
};

export default Cart;
