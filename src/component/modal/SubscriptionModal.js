import React, { useState, useEffect } from "react";
import MainModal from "./MainModal";
import Image from "next/image";
import Weekly from "@component/subscription/Weekly";
import Monthly from "@component/subscription/Monthly";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
  isRecurring,
  recurringContinuty,
  recurringInterval,
  recurringUnitTime,
} from "src/recoil/recurringOrder";

const criteria = [
  { value: "week", text: "Weekly" },
  { value: "month", text: "Monthly" },
];

const SubscriptionModal = ({ modalOpen, setModalOpen }) => {
  // Recurring States
  const [isSelect, setIsSelect] = useRecoilState(recurringUnitTime);
  const [timeIntarval, setTimeIntarval] = useRecoilState(recurringInterval);
  const [recurringErr, setRecurringErr] = useState("");
  const continuty = useSetRecoilState(recurringContinuty);
  const recurring = useSetRecoilState(isRecurring);

  let component = null;

  // Handle Criteria
  const handleCriteria = (item) => {
    setIsSelect(item);
  };

  // Handle Is Recurring
  const applyRecurring = () => {
    if (timeIntarval.length < 1) {
      setRecurringErr("Please select days or weeks!");
      return;
    }
    recurring(true);
    setModalOpen(false);
    setRecurringErr("");
  };

  const cancelRecurring = () => {
    recurring(false);
    setModalOpen(false);
    setRecurringErr("");
  };

  useEffect(() => {
    setTimeIntarval([]);
    continuty(1);
  }, [isSelect]);

  return (
    <MainModal modalOpen={modalOpen} setModalOpen={setModalOpen}>
      <div className="inline-block overflow-y-auto h-full w-full md:w-8/12 lg:w-6/12 align-middle transition-all transform bg-white shadow-xl text-left rounded-xl p-5 lg:py-5 lg:px-10 ">
        <div>
          <div className="text-center">
            <Image
              width={150}
              height={50}
              src="/logo/rsz_1unimart_logo_.png"
              alt="logo"
              objectFit="cover"
            />
          </div>
          <div className="flex justify-center items-center gap-10">
            {criteria.map((item, index) => (
              <span
                key={index}
                onClick={() => handleCriteria(item.value)}
                className={
                  item.value === isSelect
                    ? "border border-solid border-green-700 bg-green-700 text-white text-lg rounded-full px-3 py-0.5 cursor-pointer"
                    : "border border-solid border-green-700 rounded-full text-lg px-3 py-0.5 cursor-pointer"
                }
              >
                {item.text}
              </span>
            ))}
          </div>
          <div>
            <div>{isSelect === "month" ? <Monthly /> : <Weekly />}</div>
            <div className="flex flex-col md:flex-row justify-center items-center gap-5">
              <button
                onClick={applyRecurring}
                className="bg-green-700 transition-all rounded-full py-2 text-center text-sm font-serif font-medium text-white flex justify-center w-full md:w-40  mb-0"
              >
                Apply Subscription
              </button>
              <button
                onClick={cancelRecurring}
                className="transition-all rounded-full py-2 text-center text-sm font-serif font-medium text-white flex justify-center w-full md:w-40 mb-0"
                style={{ background: "#fb4512" }}
              >
                Remove Subscription
              </button>
            </div>
          </div>
        </div>
      </div>
    </MainModal>
  );
};
export default React.memo(SubscriptionModal);
