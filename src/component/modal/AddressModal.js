import React, { useEffect, useState } from "react";
import Link from "next/link";
import MainModal from "./MainModal";
import InputArea, { SelectField } from "@component/form/InputArea";
import * as Yup from "yup";
import { useFormik } from "formik";
import { axiosGetExtra } from "src/comon-func/AxiosGet";
import Label from "@component/form/Label";
import axios from "axios";
import Cookies from "js-cookie";
import { notifyError, notifySuccess } from "@utils/toast";
const AddressModal = ({ modalOpen, setModalOpen, loadAddress }) => {
  // JWT Token
  const token = Cookies.get("user-auth");
  // Division, District, Zone
  const [division, setDivision] = useState([]);
  const [district, setDistrict] = useState([]);
  const [zone, setZone] = useState([]);

  // Set District
  const setDistrictData = (e) => {
    // setDistrict([]);
    setZone([]);
    const temp_data = division.filter((el) => el.id === Number(e.target.value));
    setDistrict(temp_data[0].districts);
  };

  // Set Zone
  const setZoneData = (e) => {
    axiosGetExtra(`/DeliveryAddress/getzones/${e.target.value}`, setZone);
  };

  // Form Handle
  const formik = useFormik({
    initialValues: {
      name: "",
      houseNo: "",
      detailAddress: "",
      district: "",
      zip: "",
      contactNumber: "",
      roadNo: "",
      zone: "",
      division: "",
      saveAddressAs: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Full name is required!"),
      houseNo: Yup.string().required("House no. is required!"),
      detailAddress: Yup.string().required("Detail address is required!"),
      district: Yup.number().required("District is required!"),
      zip: Yup.string().required("Zip code is required!"),
      contactNumber: Yup.string().required("Contact number is required!"),
      roadNo: Yup.string().required("Road no. is required!"),
      zone: Yup.number().required("Zone is required!"),
      division: Yup.number()
        .typeError("Division is required!")
        .required("Division is required!"),
      saveAddressAs: Yup.string().required("Save address as is required!"),
    }),
    onSubmit: (values) => {
      addAddress(values);
    },
  });

  // Submit Address
  const addAddress = async (data) => {
    try {
      await axios({
        method: "POST",
        url: "/DeliveryAddress",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          name: data.name,
          houseNo: data.houseNo,
          detailAddress: data.detailAddress,
          district: Number(data.district),
          zip: data.zip,
          contactNumber: data.contactNumber,
          roadNo: data.roadNo,
          zone: Number(data.zone),
          division: Number(data.division),
          saveAddressAs: data.saveAddressAs,
        },
      });
      notifySuccess("Address Added Successfully!");
      setModalOpen(false);
      formik.resetForm();
      setZone([]);
      setDistrict([]);
      loadAddress();
    } catch (error) {
      notifyError("Something Went Wrong!");
      setModalOpen(false);
      console.error(error.response);
    }
  };

  // Page On Load
  useEffect(() => {
    axiosGetExtra("/DeliveryAddress/getaddress", setDivision);
  }, []);
  return (
    <MainModal modalOpen={modalOpen} setModalOpen={setModalOpen}>
      <div className="inline-block overflow-y-auto h-full w-full md:w-8/12 align-middle transition-all transform bg-white shadow-xl text-left rounded-xl p-5 lg:py-5 lg:px-10 ">
        <form onSubmit={formik.handleSubmit}>
          {/* Parsonal Detail */}
          <div className="form-group">
            <h2 className="font-semibold font-serif text-2xl md:text-left text-center text-gray-700 pb-3">
              Add New Address
            </h2>
            <div className="grid grid-cols-6 gap-6">
              <div className="col-span-6 sm:col-span-3">
                <Label label={"Full Name"} />

                <div className="relative">
                  <input
                    type="text"
                    placeholder="Full Name"
                    name="name"
                    id="name"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.name}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                {formik.touched.name && formik.errors.name ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.name}
                  </p>
                ) : null}
              </div>
              <div className="col-span-6 sm:col-span-3">
                <Label label={"Contact Number"} />

                <div className="relative">
                  <input
                    type="text"
                    placeholder="017xxxxxxxx"
                    name="contactNumber"
                    id="contactNumber"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.contactNumber}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                {formik.touched.contactNumber && formik.errors.contactNumber ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.contactNumber}
                  </p>
                ) : null}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <Label label={"House No / Flat No"} />

                <div className="relative">
                  <input
                    type="text"
                    placeholder="Youn house or flat no"
                    name="houseNo"
                    id="houseNo"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.houseNo}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                {formik.touched.houseNo && formik.errors.houseNo ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.houseNo}
                  </p>
                ) : null}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <Label label={"Road No / Road Name"} />

                <div className="relative">
                  <input
                    type="text"
                    placeholder="Road no or name"
                    name="roadNo"
                    id="roadNo"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.roadNo}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                {formik.touched.roadNo && formik.errors.roadNo ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.roadNo}
                  </p>
                ) : null}
              </div>
              <div className="col-span-6 sm:col-span-3">
                <Label label={"Detail Address / Nearby Landmark"} />

                <div className="relative">
                  <input
                    type="text"
                    placeholder="Eg: Opposite of Dhanmondi Thana"
                    name="detailAddress"
                    id="detailAddress"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.detailAddress}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                {formik.touched.detailAddress && formik.errors.detailAddress ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.detailAddress}
                  </p>
                ) : null}
              </div>
              <div className="col-span-6 sm:col-span-3">
                <Label label={"Zip Code"} />

                <div className="relative">
                  <input
                    type="text"
                    placeholder="Zip Code"
                    name="zip"
                    id="zip"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.zip}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                {formik.touched.zip && formik.errors.zip ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.zip}
                  </p>
                ) : null}
              </div>
              <div className="col-span-6 sm:col-span-3">
                <Label label={"Division"} />

                <div className="relative">
                  <select
                    name="division"
                    id="division"
                    onChange={(e) => {
                      formik.handleChange(e);
                      setDistrictData(e);
                    }}
                    onBlur={formik.handleBlur}
                    value={formik.values.division}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  >
                    <option value="">Choose Division...</option>
                    {division.map((el) => (
                      <option key={el.id} value={el.id}>
                        {el.name}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.division && formik.errors.division ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.division}
                  </p>
                ) : null}
              </div>

              <div className="col-span-6 sm:col-span-3">
                <Label label={"District"} />

                <div className="relative">
                  <select
                    name="district"
                    id="district"
                    onChange={(e) => {
                      formik.handleChange(e);
                      setZoneData(e);
                    }}
                    onBlur={formik.handleBlur}
                    value={formik.values.district}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                    disabled={district.length < 1 ? true : false}
                  >
                    <option value="">Choose District...</option>
                    {district.map((el) => (
                      <option
                        key={el.id}
                        value={el.id}
                        // onClick={() => handleDistrict(division)}
                      >
                        {el.name}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.district && formik.errors.district ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.district}
                  </p>
                ) : null}
              </div>
              <div className="col-span-6 sm:col-span-3">
                <Label label={"Zone"} />

                <div className="relative">
                  <select
                    name="zone"
                    id="zone"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.zone}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  >
                    <option value="">Choose Zone...</option>
                    {zone.map((el) => (
                      <option
                        key={el.id}
                        value={el.id}
                        // onClick={() => handleDistrict(division)}
                      >
                        {el.zoneName}
                      </option>
                    ))}
                  </select>
                </div>
                {formik.touched.zone && formik.errors.zone ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.zone}
                  </p>
                ) : null}
              </div>
              <div className="col-span-6 sm:col-span-3">
                <Label label={"Save As"} />

                <div className="relative">
                  <input
                    type="text"
                    placeholder="Home/Office etc"
                    name="saveAddressAs"
                    id="saveAddressAs"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.saveAddressAs}
                    className="py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                {formik.touched.saveAddressAs && formik.errors.saveAddressAs ? (
                  <p style={{ color: "red", fontSize: "14px" }}>
                    {formik.errors.saveAddressAs}
                  </p>
                ) : null}
              </div>
            </div>
          </div>
          <div className=" w-full lg:w-3/6 mx-auto flex justify-center items-center mt-5">
            <button
              type="submit"
              // disabled={isEmpty || isCheckoutSubmit}
              className="bg-green-500 hover:bg-green-600 border border-green-500 transition-all rounded py-2.5 text-center text-sm font-serif font-medium text-white flex justify-center w-full"
            >
              Submit{" "}
              <span className="text-xl ml-2"> {/* <IoArrowForward /> */}</span>
            </button>
          </div>
          {/* </div> */}
        </form>
      </div>
    </MainModal>
  );
};
export default React.memo(AddressModal);
