import React, { useEffect, useContext } from "react";
import MainModal from "./MainModal";
import Image from "next/image";
import useLocationSubmit from "@hooks/useLocationSubmit";
import Error from "@component/form/Error";
import { UserContext } from "@context/UserContext";

import { useRecoilState } from "recoil";
import { locationModal, locationName } from "src/recoil/locationHandler";

const LandingPageModal = ({ stores }) => {
  const [modalOpen, setModalOpen] = useRecoilState(locationModal);
  //const [location, setLocation] = useRecoilState(locationName);

  const {
    state: { storeId },
  } = useContext(UserContext);

  const { handleSubmit, submitHandler, register, errors } = useLocationSubmit();

  const handleLocation = (e) => {
    // const loca = stores.find((i) => i.storeId === item.storeId);
    let select = e.target.value;
    // let chooice = [];

    // chooice = [].map.call(select.selectedOptions, (option) => option.value);
    //console.log(select);
  };

  useEffect(() => {
    storeId ? setModalOpen(false) : setModalOpen(true);
  }, [stores]);
  return (
    <MainModal modalOpen={modalOpen} setModalOpen={() => setModalOpen(false)}>
      <div className="inline-block overflow-y-auto h-full lg:h-96 w-full sm:w-8/12 lg:w-6/12 align-middle transition-all transform bg-white shadow-xl text-left rounded-xl p-5 lg:py-5 lg:px-10 ">
        <div className="text-center">
          <div>
            <Image
              width={160}
              height={70}
              src="/logo/rsz_1unimart_logo_.png"
              alt="logo"
              objectFit="cover"
            />
          </div>
          <p
            className="font-semibold font-serif text-2xl  text-center pb-8"
            style={{ color: "#fb4512" }}
          >
            {" "}
            Please Select Your Delivery Location
          </p>
          <form onSubmit={handleSubmit(submitHandler)}>
            <div className="w-full md:w-80 text-center mx-auto mb-8">
              <select
                {...register("location", {
                  required: `Your location is required!`,
                })}
                // register={register}
                // label="Zone"
                name="location"
                // multiple
                // size={6}
                onChange={(event) => handleLocation(event)}
                className="py-2 px-4 md:px-5 w-full form-select appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-10 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-300 focus:outline-none focus:border-green-700 h-10 md:h-10"
              >
                {stores?.map((item) => (
                  <option
                    key={item.id}
                    //value={[[item.storeId], [item.area]]}
                    value={[item.storeId, item.area]}
                    //onSelect={() => handleLocation(item)}
                  >
                    {item.area}
                  </option>
                ))}
              </select>
              <Error errorName={errors.zone} />
            </div>
            <button
              type="submit"
              className="bg-green-700 hover:bg-green-600 border border-green-500 transition-all rounded py-2 text-center text-sm font-serif font-medium text-white flex justify-center w-full md:w-80 mx-auto mb-5"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </MainModal>
  );
};
export default React.memo(LandingPageModal);
