import React, { useState, useContext } from "react";
import Link from "next/link";
import Image from "next/image";
import { FiPlus, FiMinus } from "react-icons/fi";
import { useCart } from "react-use-cart";

import MainModal from "@component/modal/MainModal";
import Tags from "@component/common/Tags";
import Stock from "@component/common/Stock";
import Price from "@component/common/Price";
import { UserContext } from "@context/UserContext";
import { notifySuccess, notifyError } from "@utils/toast";
import { useRecoilState } from "recoil";
import {
  allCartItems,
  totalCartAmount,
  totalItems,
} from "src/recoil/cartHandler";

const ProductModal = ({ modalOpen, setModalOpen, product, image }) => {
  ////////////////////////
  ////////////////////////
  ////////////////////////
  ////////////////////////
  ////////////////////////
  ////////////////////////
  const [carts, setCarts] = useRecoilState(allCartItems);
  const [cartsItems, setCartsItems] = useRecoilState(totalItems);
  const [cartsAmount, setCartsAmount] = useRecoilState(totalCartAmount);

  // Check If It's In Cart
  const cartIn = (product) => {
    if (product?.discAmount !== undefined) {
      return carts.campaignDTOs?.some(
        (el) => el.productId === product.productId
      );
    }
    if (product?.discAmount === undefined) {
      return carts.orderProductsDTOs?.some(
        (el) => el.productId === product.productId
      );
    }
  };

  // Quantity In Cart
  const cartOneItemQuantity = (product) => {
    if (product?.discAmount !== undefined) {
      const cartIndex = carts.campaignDTOs?.findIndex(
        (el) => el.productId === product.productId
      );
      return carts.campaignDTOs[cartIndex]?.quantity;
    }
    if (product?.discAmount === undefined) {
      const cartIndex = carts.orderProductsDTOs?.findIndex(
        (el) => el.productId === product.productId
      );
      return carts.orderProductsDTOs[cartIndex]?.quantity;
    }
  };

  // First Add
  const addToCartFirstTime = (data) => {
    const cartOldData = JSON.parse(
      localStorage.getItem("uni-cart") ||
        JSON.stringify({
          orderProductsDTOs: [],
          campaignDTOs: [],
        })
    );
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items") || 0);
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount") || 0);

    const cartNewData = {
      productId: data.productId,
      productName: data.productName,
      quantity: 1,
      image: data.images[0]?.url,
    };

    if (data?.discAmount !== undefined) {
      cartOldData.campaignDTOs.push({
        ...cartNewData,
        campaignId: 22,
        productPrice: data.atrDiscAmount,
      });
      setCartsAmount((old) => old + data.atrDiscAmount);
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + data.atrDiscAmount)
      );
    }
    if (data?.discAmount === undefined) {
      cartOldData.orderProductsDTOs.push({
        ...cartNewData,
        productPrice: data.price,
      });
      setCartsAmount((old) => old + data.price);
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + data.price)
      );
    }

    // State
    setCarts(cartOldData);
    setCartsItems((old) => old + 1);

    // LocalStorage
    localStorage.setItem("uni-cart", JSON.stringify(cartOldData));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem + 1));
  };

  // Plus One
  const addToCartPlusOne = (product) => {
    console.log(product);
    console.log(product.productPrice);
    let cart = JSON.parse(JSON.stringify(carts));
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items"));
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount"));

    if (product?.discAmount !== undefined) {
      const cartIndex = cart.campaignDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.campaignDTOs[cartIndex].quantity += 1;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + product.atrDiscAmount)
      );
      setCartsAmount((old) => old + product.atrDiscAmount);
    }
    if (product?.discAmount === undefined) {
      const cartIndex = cart.orderProductsDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.orderProductsDTOs[cartIndex].quantity += 1;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + product.price)
      );
      setCartsAmount((old) => old + product.price);
    }

    localStorage.setItem("uni-cart", JSON.stringify(cart));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem + 1));

    setCarts(cart);
    setCartsItems((old) => old + 1);
  };

  // Minus One
  const addToCartMinusOne = (product) => {
    let cart = JSON.parse(JSON.stringify(carts));
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items"));
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount"));

    if (product?.discAmount !== undefined) {
      const cartIndex = cart.campaignDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.campaignDTOs[cartIndex].quantity -= 1;
      const cartQuantity = cart.campaignDTOs[cartIndex].quantity;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount - product.atrDiscAmount)
      );
      setCartsAmount((old) => old - product.atrDiscAmount);
      if (cartQuantity < 1) {
        let campProdCart = cart.campaignDTOs;
        campProdCart = campProdCart.filter(
          (el) => el.productId !== product.productId
        );
        cart.campaignDTOs = campProdCart;
      }
    }
    if (product?.discAmount === undefined) {
      const cartIndex = cart.orderProductsDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.orderProductsDTOs[cartIndex].quantity -= 1;
      const cartQuantity = cart.orderProductsDTOs[cartIndex].quantity;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount - product.price)
      );
      setCartsAmount((old) => old - product.price);
      if (cartQuantity < 1) {
        let orderProdCart = cart.orderProductsDTOs;
        orderProdCart = orderProdCart.filter(
          (el) => el.productId !== product.productId
        );
        cart.orderProductsDTOs = orderProdCart;
      }
    }

    localStorage.setItem("uni-cart", JSON.stringify(cart));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem - 1));

    setCarts(cart);
    setCartsItems((old) => old - 1);
  };
  return (
    <MainModal modalOpen={modalOpen} setModalOpen={setModalOpen}>
      <div className="inline-block overflow-y-auto h-full align-middle transition-all transform bg-white shadow-xl rounded-2xl">
        <div className="flex flex-col lg:flex-row md:flex-row justify-center items-center w-full max-w-4xl overflow-hidden">
          <div className="flex-shrink-0  cursor-pointer">
            {/* <Link href={`/product/${product.productSKU}`} passHref> */}
            <Image
              src={`https://test.bespokeit.io/${product.images[0].url}`}
              width={400}
              height={400}
              alt={product.productName}
            />
            {/* </Link> */}
            {/* {product.image.length > 1 ? (
              <div className="flex justify-center items-center text-center">
                {product.image.map((item, index) => (
                  <div
                    key={index}
                    className="w-full border border-solid border-gray-300 rounded-md m-2"
                    onClick={() => setTab(index)}
                  >
                    <Image
                      src={item}
                      alt="Image"
                      width="100%"
                      height="100%"
                      objectFit="cover"
                      className=" border border-solid border-red-600"
                    />
                  </div>
                ))}
              </div>
            ) : null} */}
          </div>

          <div className="w-full flex flex-col p-5 md:p-8 text-left">
            <div className="mb-2 md:mb-2.5 block -mt-1.5">
              <Link href={`/product/${product.productSKU}`} passHref>
                <h1
                  onClick={() => setModalOpen(false)}
                  className="text-heading text-lg md:text-xl lg:text-2xl font-semibold font-serif hover:text-black cursor-pointer"
                >
                  {product.productName}
                </h1>
              </Link>

              {/* <Stock product={product} /> */}
            </div>
            <p className="text-sm leading-6 text-gray-500 md:leading-7 line-clamp-3">
              {product.description}
            </p>
            <div className="flex items-center mt-4">
              <Price product={product} />
            </div>

            <div className="flex items-center mt-4">
              <div className="flex items-center justify-between space-s-3 sm:space-s-4 w-full">
                {cartIn(product) && (
                  <div className="group flex items-center justify-between rounded-md overflow-hidden flex-shrink-0 border h-11 md:h-12 border-gray-300">
                    <button
                      onClick={() => addToCartMinusOne(product)}
                      disabled={cartOneItemQuantity(product) === 0}
                      className="flex items-center justify-center flex-shrink-0 h-full transition ease-in-out duration-300 focus:outline-none w-8 md:w-12 text-heading border-e border-gray-300 hover:text-gray-500"
                    >
                      <span className="text-dark text-base">
                        <FiMinus />
                      </span>
                    </button>
                    <p className="font-semibold flex items-center justify-center h-full  transition-colors duration-250 ease-in-out cursor-default flex-shrink-0 text-base text-heading w-8  md:w-20 xl:w-24">
                      {cartOneItemQuantity(product)}
                    </p>
                    <button
                      onClick={() => addToCartPlusOne(product)}
                      className="flex items-center justify-center h-full flex-shrink-0 transition ease-in-out duration-300 focus:outline-none w-8 md:w-12 text-heading border-s border-gray-300 hover:text-gray-500"
                    >
                      <span className="text-dark text-base">
                        <FiPlus />
                      </span>
                    </button>
                  </div>
                )}
                {!cartIn(product) && (
                  <button
                    onClick={() => addToCartFirstTime(product)}
                    className="text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold font-serif text-center justify-center border-0 border-transparent rounded-md focus-visible:outline-none focus:outline-none text-white px-4 md:px-6 lg:px-8 py-4 md:py-3.5 lg:py-4 hover:text-white bg-green-500 hover:bg-green-600 w-full h-12"
                  >
                    Add To Cart
                  </button>
                )}
              </div>
            </div>
            <div className="flex flex-col mt-4">
              <span className="font-serif font-semibold py-1 text-sm d-block">
                <span className="text-gray-700">Category:</span>{" "}
                <span className="text-gray-500">{product.parentCatName}</span>
              </span>
              <Tags product={product} />
            </div>
          </div>
        </div>
      </div>
    </MainModal>
  );
};

export default React.memo(ProductModal);
