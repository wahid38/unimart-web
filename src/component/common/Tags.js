import React from "react";

const Tags = ({ product }) => {
  return (
    <>
      <div className="flex flex-row">
        {product.tags.map((el) => (
          <span
            key={el.id}
            className="bg-gray-100 border-0 text-gray-500 rounded-full inline-flex items-center justify-center px-2 py-1 text-xs font-semibold font-serif mt-2 mr-2"
          >
            {el.tags}
          </span>
        ))}
      </div>
    </>
  );
};

export default Tags;
