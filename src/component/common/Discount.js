import React from "react";

const Discount = ({ product, slug }) => {
  return (
    <>
      {product.discAmount && (
        <span
          className={
            slug
              ? "text-dark text-sm bg-orange-500 text-white py-1 px-2 rounded font-medium z-10 right-8 top-1"
              : " absolute text-dark text-xs bg-orange-500 text-white py-1 px-2 rounded font-medium z-10 right-8 top-1"
          }
        >
          {/* {product.discAmount.toFixed(0)}% Off */}
          {product.discAmount}% Off
        </span>
      )}
    </>
  );
};

export default Discount;
