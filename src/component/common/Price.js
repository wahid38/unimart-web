import React from "react";

const Price = ({ product, card }) => {
  let promoPrice = product.originalPrice;
  let discounAmount = null;
  if (product.discount) {
    discounAmount = (product.originalPrice * product.discount) / 100;
    promoPrice = Math.ceil(product.originalPrice - discounAmount);
  }
  //promoPrice = product.originalPrice * product.discount
  return (
    <div className="font-serif product-price font-bold">
      {product.discAmount ? (
        <span
          className={
            card
              ? "inline-block text-lg font-semibold text-gray-800"
              : "inline-block text-2xl"
          }
        >
          &#2547;{product.atrDiscAmount}
        </span>
      ) : (
        <span
          className={
            card
              ? "inline-block text-lg font-semibold text-gray-800"
              : "inline-block text-2xl"
          }
        >
          &#2547;{product.price}
        </span>
      )}
      {product.discAmount ? (
        <del
          className={
            card
              ? "sm:text-sm font-normal text-base text-gray-400 ml-1"
              : "text-lg font-normal text-gray-400 ml-1"
          }
        >
          &#2547;{product.regularPrice}
        </del>
      ) : null}
    </div>
  );
};

export default Price;
