import { useContext, useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { IoClose } from "react-icons/io5";
import { HiOutlineArrowLeft } from "react-icons/hi";

//internal import
import Loading from "@component/preloader/Loading";
import { SidebarContext } from "@context/SidebarContext";
import CategoryCard from "@component/category/CategoryCard";
import logo from "./rsz_1unimart_logo_.png";
import { axiosGet } from "src/comon-func/AxiosGet";
const Category = () => {
  const { categoryDrawerOpen, closeCategoryDrawer } =
    useContext(SidebarContext);

  const [categories, setCategories] = useState([]);

  //Page On Load
  useEffect(() => {
    axiosGet("/Category", setCategories);
  });

  return (
    <div className="flex flex-col w-full h-full bg-white cursor-pointer scrollbar-hide">
      {categoryDrawerOpen && (
        <div className="w-full flex justify-between items-center h-16 px-6 py-4 bg-white text-white border-b border-green-300">
          {/* <h2 className="font-semibold font-serif text-lg m-0 text-heading flex align-center"> */}
          <Link href="/">
            <a className="mr-10">
              <Image width={80} height={45} src={logo} alt="logo" />
            </a>
          </Link>
          {/* </h2> */}
          <button
            onClick={closeCategoryDrawer}
            className="flex text-xl items-center justify-center w-8 h-8 rounded-full bg-gray-100 text-green-500 p-2 focus:outline-none transition-opacity hover:text-red-600"
            aria-label="close"
          >
            <HiOutlineArrowLeft />
          </button>
        </div>
      )}
      <div className="overflow-y-scroll scrollbar-hide w-full max-h-full">
        {categoryDrawerOpen && (
          <h2 className="font-semibold font-serif text-lg m-0 text-heading flex align-center border-b px-8 py-1">
            All Categories
          </h2>
        )}

        {
          // error ? (
          //   <p className="flex justify-center align-middle items-center m-auto text-xl text-red-500">
          //     <span> {error}</span>
          //   </p>
          // ) :
          categories.length === 0 ? (
            <Loading loading={categories.length < 1} />
          ) : (
            <div className="relative grid gap-2 px-6 py-3">
              {categories?.map((category) => (
                <CategoryCard
                  key={category.id}
                  title={category.parentCat}
                  icon={category.imageUrl}
                  nested={category.child}
                />
              ))}
            </div>
          )
        }

        {/* {categoryDrawerOpen && (
          <div className="relative grid gap-2 mt-5">
            <h3 className="font-semibold font-serif text-lg m-0 text-heading flex align-center border-b px-8 py-3">
              Pages
            </h3>
            <div className="relative grid gap-1 p-6">
              {pages.map((item) => (
                <a
                  key={item.title}
                  href={item.href}
                  className="p-2 flex font-serif items-center rounded-md hover:bg-gray-50 w-full hover:text-green-600"
                >
                  <item.icon
                    className="flex-shrink-0 h-4 w-4"
                    aria-hidden="true"
                  />
                  <p className="inline-flex items-center justify-between ml-2 text-sm font-medium w-full hover:text-green-600">
                    {item.title}
                  </p>
                </a>
              ))}
            </div>
          </div>
        )} */}
      </div>
    </div>
  );
};

export default Category;
