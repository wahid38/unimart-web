import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { axiosGet } from "src/comon-func/AxiosGet";

const FeatureCategory = () => {
  const router = useRouter();
  const [categories, setCategories] = useState([]);

  // Page On Load
  useEffect(() => {
    axiosGet("/Category", setCategories);
  }, []);
  return (
    <>
      <ul className="grid grid-cols-2 gap-2 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-4 xl:grid-cols-4">
        {categories?.slice(0, 8).map((item) => (
          <li className="group" key={item.id}>
            <div
              onClick={() =>
                router.push(
                  `/search/${item.id}?Category=${item.parentCat
                    .toLowerCase()
                    .replace("&", "")
                    .split(" ")
                    .join("-")}`
                )
              }
              className="border border-gray-100 bg-white rounded-lg p-4 block cursor-pointer transition duration-200 ease-linear transform group-hover:scale-105"
            >
              <div className="flex items-center">
                <Image
                  src={`https://test.bespokeit.io/${item.imageUrl}`}
                  alt={item.parentCat}
                  width={35}
                  height={35}
                />
                <h3 className="pl-3 lg:pl-4 text-sm text-gray-600 font-serif font-medium leading-tight">
                  {item.parentCat}
                </h3>
              </div>
            </div>
          </li>
        ))}
      </ul>
      {/* )} */}
    </>
  );
};

export default FeatureCategory;
