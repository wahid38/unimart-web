import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import useFilter from "@hooks/useFilter";
import { products } from "@utils/products";
import ProductCard from "@component/product/ProductCard";
import { useCart } from "react-use-cart";
import { notifySuccess } from "@utils/toast";
import { camapignProduct } from "@utils/campaign";
import OfferTimer from "./OfferTimer";
import { useRecoilValue } from "recoil";
import { campaignProduct } from "src/recoil/productHandler";
import dayjs from "dayjs";
import { axiosGet, axiosGetExtra } from "src/comon-func/AxiosGet";

const endTime = "2022-02-31T06:00:00Z";

export default function Campaign() {
  // Products
  const [products, setProducts] = useState([]);

  // On page Load
  useEffect(() => {
    axiosGet(`/Campaign/getcampaigns/7`, setProducts);
  }, []);
  return (
    <div>
      {/* {camapignProduct.map((comboProduct) => (
        <div className="w-full" key={comboProduct.slug}>
          <h2 className="pt-10 text-xl text-center lg:text-2xl mb-2 font-serif font-semibold">
            {comboProduct.title}
          </h2>
          <div className="relative w-full" style={{ height: "33vw" }}>
            <Image
              src={comboProduct.image[0]}
              alt="Offer Banner"
              layout="fill"
              objectFit="contain"
            />
            <div
              className="absolute right-0 bottom-0 mr-3 mb-2 lg:mr-10 lg:mb-5"
              onClick={() => handleAddItem(comboProduct)}
            >
              <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-2  py-2 md:px-5 md:py-2 hover:text-white hover:bg-green-600  text-xs lg:text-base ">
                Add to cart
              </a>
            </div>
          </div>
        </div>
      ))} */}

      {/* discounted products */}
      {products.map((el) => (
        <div key={el.id}>
          {dayjs().isAfter(dayjs(el.endDate)) ? null : (
            <div className="bg-gray-50 lg:py-5 pt-5 mx-auto max-w-screen-2xl px-3 sm:px-10">
              <div className="mb-5 flex justify-center">
                <div className="text-center w-full lg:w-2/5">
                  <h2 className="text-xl lg:text-2xl mb-2 font-serif font-semibold">
                    {el.campaignName}
                  </h2>
                  <div>
                    <span className="inline-block mb-2">
                      <div className="flex items-center font-semibold">
                        <OfferTimer expiryTimestamp={new Date(el.endDate)} />
                      </div>
                    </span>
                  </div>
                  {/* <p className="text-base font-sans text-gray-600 leading-6">
                      See Our latest discounted products below. Choose your daily
                      needs from here and get a special discount with free shipping.
                    </p> */}
                </div>
              </div>
              <div className="flex">
                <div className="w-full">
                  <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-6 xl:grid-cols-6 2xl:grid-cols-6 gap-2 md:gap-3 lg:gap-3">
                    {el.products.map((product) => (
                      <ProductCard
                        key={product.id}
                        product={product}
                        // image={campaign.imageUrl
                        //   .slice(0, 1)
                        //   .map((img) => img.url)}
                      />
                    ))}
                  </div>
                </div>
              </div>
              {/* {productData.length > visibleProduct && (
                <div
                  className="sm:text-right"
                  onClick={() => setVisibleProduct((pre) => pre + 12)}
                >
                  <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-5  py-3 hover:text-white hover:bg-green-600 h-9 mt-3 text-sm lg:text-sm w-full sm:w-auto">
                    Show More...
                  </a>
                </div>
              )} */}
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
