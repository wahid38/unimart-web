import React, { useState } from "react";
import { Switch } from "@headlessui/react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { ImFacebook, ImGoogle } from "react-icons/im";
import { FiLock, FiMail, FiUser, FiPhone } from "react-icons/fi";

import Label from "@component/form/Label";
import { userLogin, userRegister } from "src/comon-func/Authentication";

const Common = ({ onShowRegister, setModalOpen }) => {
  const [enabled, setEnabled] = useState(false);

  // Register Schema and Form
  const registerSchema = Yup.object({
    firstName: Yup.string().required("First name is required!"),
    lastName: Yup.string().required("Last name is required!"),
    email: Yup.string()
      .email("Enter a valid email address!")
      .required("Last name is required!"),
    password: Yup.string()
      .min(
        8,
        "Password is too short. Password must be at least 8 character long!"
      )
      .required("Password is required!"),
    phoneNumber: Yup.string()
      .matches(
        /(^(\+88|88)?(01){1}[3456789]{1}(\d){8})$/,
        "Enter a valid phone number!"
      )
      .required("Phone number is required!"),
  });

  const registerForm = {
    firstName: "",
    lastName: "",
    phoneNumber: "",
    email: "",
    password: "",
  };

  // Login Schema and Form
  const loginSchema = Yup.object({
    password: Yup.string()
      .min(
        8,
        "Password is too short. Password must be at least 8 character long!"
      )
      .required("Password is required!"),
    phoneNumber: Yup.string()
      .matches(
        /(^(\+88|88)?(01){1}[3456789]{1}(\d){8})$/,
        "Enter a valid phone number!"
      )
      .required("Phone number is required!"),
  });

  const loginForm = {
    phoneNumber: "",
    password: "",
  };

  // Handle Form
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: onShowRegister === undefined ? registerForm : loginForm,
    validationSchema:
      onShowRegister === undefined ? registerSchema : loginSchema,
    onSubmit: (values) => {
      if (onShowRegister === undefined) {
        userRegister(values, setModalOpen);
        return;
      }
      userLogin(values, setModalOpen);
    },
  });

  return (
    <>
      <form
        onSubmit={formik.handleSubmit}
        className="flex flex-col justify-center"
      >
        <div className="grid grid-cols-1 gap-5">
          {!onShowRegister && (
            <div>
              {/* First Name Field */}
              <div className="form-group">
                <Label label={"First Name"} />

                <div className="relative">
                  <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                    <span className="text-gray-800 focus-within:text-gray-900 sm:text-base">
                      <FiUser />{" "}
                    </span>
                  </div>

                  <input
                    type="text"
                    placeholder="First Name"
                    name="firstName"
                    id="firstName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.firstName}
                    className="py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                <p style={{ color: "red", fontSize: "14px" }}>
                  {formik.errors.firstName}
                </p>
              </div>
              <br />
              {/* Last Name */}
              <div className="form-group">
                <Label label={"Last Name"} />

                <div className="relative">
                  <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                    <span className="text-gray-800 focus-within:text-gray-900 sm:text-base">
                      <FiUser />{" "}
                    </span>
                  </div>

                  <input
                    type="text"
                    placeholder="Last Name"
                    name="lastName"
                    id="lastName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lastName}
                    className="py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                  />
                </div>
                <p style={{ color: "red", fontSize: "14px" }}>
                  {formik.errors.lastName}
                </p>
              </div>
            </div>
          )}
          {/* Email Field */}
          {!onShowRegister && (
            <div className="form-group">
              <Label label={"Email"} />

              <div className="relative">
                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                  <span className="text-gray-800 focus-within:text-gray-900 sm:text-base">
                    <FiMail />{" "}
                  </span>
                </div>

                <input
                  type="email"
                  placeholder="Email"
                  name="email"
                  id="email"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                  className="py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                />
              </div>
              <p style={{ color: "red", fontSize: "14px" }}>
                {formik.errors.email}
              </p>
            </div>
          )}

          {/* Phone Number Field For Login */}
          <div className="form-group">
            <Label label={"Phone Number"} />

            <div className="relative">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-800 focus-within:text-gray-900 sm:text-base">
                  <FiPhone />{" "}
                </span>
              </div>

              <input
                type="text"
                placeholder="01XXXXXXXXX"
                name="phoneNumber"
                id="phoneNumber"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.phoneNumber}
                className="py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
              />
            </div>
            <p style={{ color: "red", fontSize: "14px" }}>
              {formik.errors.phoneNumber}
            </p>
          </div>

          {/* Password Field For Register */}

          <div className="form-group">
            <Label label={"Password"} />

            <div className="relative">
              <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-800 focus-within:text-gray-900 sm:text-base">
                  <FiLock />{" "}
                </span>
              </div>

              <input
                type="password"
                placeholder="Password"
                name="password"
                id="password"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
                className="py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
              />
            </div>
            <p style={{ color: "red", fontSize: "14px" }}>
              {formik.errors.password}
            </p>
          </div>
          <div className="flex items-center justify-between">
            <div className="flex items-center flex-shrink-0">
              <Switch
                checked={enabled}
                onChange={setEnabled}
                className={`${
                  enabled ? "bg-blue-600" : "bg-gray-200"
                } relative inline-flex items-center h-6 rounded-full w-11`}
              >
                <span className="sr-only">Enable notifications</span>
                <span
                  className={`${
                    enabled ? "translate-x-6" : "translate-x-1"
                  } inline-block w-4 h-4 transform bg-white rounded-full`}
                />
              </Switch>
            </div>
            <div className="flex ms-auto">
              <button
                type="button"
                className="text-end text-sm text-heading ps-3 underline hover:no-underline focus:outline-none"
              >
                Forgot password?
              </button>
            </div>
          </div>
          <button
            type="submit"
            className="w-full text-center py-3 rounded bg-green-500 text-white hover:bg-green-600 transition-all focus:outline-none my-1"
          >
            {onShowRegister ? <span>Login</span> : <span>Register</span>}
          </button>
        </div>
      </form>
      <div className="my-8 after:bg-gray-100 before:bg-gray-100 fo10t-sans text-center font-medium">
        OR
      </div>

      {/* <div className="flex justify-between flex-col lg:flex-row">
        <button className="text-sm inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold font-serif text-center justify-center rounded-md focus:outline-none text-gray-600 bg-gray-100 shadow-sm md:px-2 my-1 sm:my-1 md:my-1 lg:my-0 lg:px-3 py-4 md:py-3.5 lg:py-4 hover:text-white hover:bg-blue-600 h-11 md:h-12 w-full mr-2">
          <ImFacebook /> <span className="ml-2">Login With Facebook</span>
        </button>

        <GoogleLogin
          clientId={`${process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}`}
          render={(renderProps) => (
            <button
              className="text-sm inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold font-serif text-center justify-center rounded-md focus:outline-none text-gray-600 bg-gray-100 shadow-sm md:px-2 my-1 sm:my-1 md:my-1 lg:my-0 lg:px-3 py-4 md:py-3.5 lg:py-4 hover:text-white hover:bg-red-500 h-11 md:h-12 w-full"
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
            >
              <ImGoogle /> <span className="ml-2">Login With Google</span>
            </button>
          )}
          onSuccess={handleGoogleSignIn}
          onFailure={handleGoogleSignIn}
          cookiePolicy={"single_host_origin"}
        />
      </div> */}
    </>
  );
};

export default Common;
