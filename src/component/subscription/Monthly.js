import React from "react";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
  recurringContinuty,
  recurringInterval,
} from "src/recoil/recurringOrder";

const dates = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
  23, 24, 25, 26, 27, 28, 29, 30, 31,
];
const monthList = [
  "1 month",
  "2 months",
  "3 months",
  "4 months",
  "5 months",
  "6 months",
  "7 months",
  "8 months",
  "9 months",
  "10 months",
  "11 months",
  "12 months",
];

export default function Monthly() {
  const [isSelect, setIsSelect] = useRecoilState(recurringInterval);
  const continuty = useSetRecoilState(recurringContinuty);

  const handleDate = (date) => {
    if (isSelect.some((el) => el === date)) {
      setIsSelect((old) => old.filter((el) => el !== date));
      return;
    }
    setIsSelect((old) => [...old, date]);
  };

  return (
    <div>
      <div className="text-center grid grid-cols-7 gap-y-1 md:gap-y-3 justify-center items-center md:w-80 mx-auto py-5">
        {dates.map((date) => (
          <span
            key={date}
            onClick={() => handleDate(date)}
            className={
              isSelect.some((el) => el === date)
                ? "border border-solid border-orange-600 bg-orange-600 text-white rounded-full text-sm p-1 w-6 h-6 flex justify-center items-center cursor-pointer"
                : "border border-solid border-green-700 rounded-full text-sm p-1 w-6 h-6 flex justify-center items-center cursor-pointer"
            }
          >
            {date}
          </span>
        ))}
      </div>
      <div className="w-full md:w-80 text-center mx-auto mb-8">
        <select
          onChange={(e) => continuty(e.target.value)}
          name="location"
          className="py-2 px-4 md:px-5 w-full form-select appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-10 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-400 focus:outline-none focus:border-green-700 h-10 md:h-10"
        >
          {monthList.map((item, index) => (
            <option key={index + 1} value={index + 1}>
              {item}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
}
