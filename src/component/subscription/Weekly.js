import React from "react";
import { useRecoilState, useSetRecoilState } from "recoil";
import {
  recurringContinuty,
  recurringInterval,
} from "src/recoil/recurringOrder";

const week = [
  { id: 1, text: "Sun" },
  { id: 2, text: "Mon" },
  { id: 3, text: "Tue" },
  { id: 4, text: "Wed" },
  { id: 5, text: "Thu" },
  { id: 6, text: "Fri" },
  { id: 7, text: "Sat" },
];
const weekList = [
  "1 week",
  "2 weeks",
  "3 weeks",
  "4 weeks",
  "5 weeks",
  "6 weeks",
  "7 weeks",
  "8 weeks",
  "9 weeks",
  "10 weeks",
  "11 weeks",
  "12 weeks",
];

export default function Weekly() {
  const [isSelect, setIsSelect] = useRecoilState(recurringInterval);
  const continuty = useSetRecoilState(recurringContinuty);

  const handleDay = (day) => {
    if (isSelect.some((el) => el === day)) {
      setIsSelect((old) => old.filter((el) => el !== day));
      return;
    }
    setIsSelect((old) => [...old, day]);
  };
  return (
    <div>
      <div className="text-center py-12 grid grid-cols-4 gap-y-5 md:grid-cols-4 md:w-4/6 mx-auto">
        {week.map((day, index) => (
          <span
            key={index}
            onClick={() => handleDay(day.id)}
            className={
              isSelect.some((el) => el === day.id)
                ? "bg-orange-600 text-white rounded-full mx-1 px-1 py-1 text-center text-sm cursor-pointer"
                : "border border-solid border-green-700 rounded-full mx-1 px-1 py-1 text-center text-sm  cursor-pointer"
            }
          >
            {day.text}
          </span>
        ))}
      </div>
      <div className="w-full md:w-80 text-center mx-auto mb-8">
        <select
          onChange={(e) => continuty(e.target.value)}
          name="location"
          className="py-2 px-4 md:px-5 w-full form-select appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-10 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-300 focus:outline-none focus:border-green-700 h-10 md:h-10"
        >
          {weekList.map((item, index) => (
            <option key={index + 1} value={index + 1}>
              {item}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
}
