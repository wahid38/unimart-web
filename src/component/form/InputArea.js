import React from "react";
import Label from "@component/form/Label";

const InputArea = ({
  register,
  defaultValue,
  name,
  label,
  type,
  placeholder,
  Icon,
}) => {
  return (
    <>
      <Label label={label} />
      <div className="relative">
        {Icon && (
          <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
            <span className="text-gray-800 focus-within:text-gray-900 sm:text-base">
              <Icon />{" "}
            </span>
          </div>
        )}
        {name === "phone" ? (
          <input
            {...register(`${name}`, {
              required: `${label} is required!`,
              pattern: {
                value: /(^(\+88|88)?(01){1}[3456789]{1}(\d){8})$/,
                message: "Invalid phone number",
              },
            })}
            defaultValue={defaultValue}
            type={type}
            placeholder={placeholder}
            name={name}
            className={
              Icon
                ? "py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                : "py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
            }
          />
        ) : (
          <input
            {...register(`${name}`, {
              required: `${label} is required!`,
            })}
            defaultValue={defaultValue}
            type={type}
            placeholder={placeholder}
            name={name}
            className={
              Icon
                ? "py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
                : "py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
            }
          />
        )}
      </div>
    </>
  );
};

export default InputArea;

export const SelectField = ({
  register,
  defaultValue,
  name,
  label,
  type,
  placeholder,
  Icon,
  value,
}) => {
  const handleDistrict = (division) => {
    console.log(division);
  };
  return (
    <>
      <Label label={label} />
      <div className="relative">
        {Icon && (
          <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
            <span className="text-gray-800 focus-within:text-gray-900 sm:text-base">
              <Icon />{" "}
            </span>
          </div>
        )}
        <select
          {...register(`${name}`, {
            required: `${label} is required!`,
          })}
          defaultValue={defaultValue}
          // type={type}
          // placeholder={placeholder}
          name={name}
          className={
            Icon
              ? "py-2 pl-10 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
              : "py-2 px-4 md:px-5 w-full appearance-none border text-sm opacity-75 text-input rounded-md placeholder-body min-h-12 transition duration-200 focus:ring-0 ease-in-out bg-white border-gray-200 focus:outline-none focus:border-green-500 h-10 md:h-10"
          }
        >
          {/* <option>{defaultValue}</option> */}
          {value.status === 5 &&
            value.result.map((division) => (
              <option
                key={division.id}
                value={division.name}
                onClick={() => handleDistrict(division)}
              >
                {division.name}
              </option>
            ))}
        </select>
      </div>
    </>
  );
};
