import { useContext, useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { IoBagAddSharp, IoAdd, IoRemove } from "react-icons/io5";
import {
  MdOutlineFavoriteBorder,
  MdOutlineFavorite,
  MdClose,
} from "react-icons/md";
//Recoil
import { isAdd } from "src/recoil/wishlistHandler";
import { useRecoilState } from "recoil";

import Price from "@component/common/Price";
import Discount from "@component/common/Discount";
import ProductModal from "@component/modal/ProductModal";
import { SidebarContext } from "@context/SidebarContext";
import { UserContext } from "@context/UserContext";
import { notifyError, notifySuccess } from "@utils/toast";
import {
  allCartItems,
  totalCartAmount,
  totalItems,
} from "src/recoil/cartHandler";
import axios from "axios";
import Cookies from "js-cookie";

const ProductCard = ({ product, image }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [isFavorit, setIsFavorit] = useState(false);
  const [addToFav, setAddToFav] = useRecoilState(isAdd);
  //console.log(image);

  const router = useRouter();

  //const img = campaign.imageUrl.map((image) => image.url);

  const {
    state: { userInfo },
  } = useContext(UserContext);

  //JWT
  const token = Cookies.get("user-auth");

  ////////////////////
  ////////////////////
  ////////////////////
  ////////////////////
  ////////////////////
  const [carts, setCarts] = useRecoilState(allCartItems);
  const [cartsItems, setCartsItems] = useRecoilState(totalItems);
  const [cartsAmount, setCartsAmount] = useRecoilState(totalCartAmount);

  // Check If It's In Cart
  const cartIn = (product) => {
    if (product?.discAmount !== undefined) {
      return carts.campaignDTOs?.some(
        (el) => el.productId === product.productId
      );
    }
    if (product?.discAmount === undefined) {
      return carts.orderProductsDTOs?.some(
        (el) => el.productId === product.productId
      );
    }
  };

  // Quantity In Cart
  const cartOneItemQuantity = (product) => {
    if (product?.discAmount !== undefined) {
      const cartIndex = carts.campaignDTOs?.findIndex(
        (el) => el.productId === product.productId
      );
      return carts.campaignDTOs[cartIndex]?.quantity;
    }
    if (product?.discAmount === undefined) {
      const cartIndex = carts.orderProductsDTOs?.findIndex(
        (el) => el.productId === product.productId
      );
      return carts.orderProductsDTOs[cartIndex]?.quantity;
    }
  };

  // First Add
  const addToCartFirstTime = (data) => {
    const cartOldData = JSON.parse(
      localStorage.getItem("uni-cart") ||
        JSON.stringify({
          orderProductsDTOs: [],
          campaignDTOs: [],
        })
    );
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items") || 0);
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount") || 0);

    const cartNewData = {
      productId: data.productId,
      productName: data.productName,
      quantity: 1,
      image: data.images[0]?.url,
    };

    if (data?.discAmount !== undefined) {
      cartOldData.campaignDTOs.push({
        ...cartNewData,
        campaignId: 22,
        productPrice: data.atrDiscAmount,
      });
      setCartsAmount((old) => old + data.atrDiscAmount);
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + data.atrDiscAmount)
      );
    }
    if (data?.discAmount === undefined) {
      cartOldData.orderProductsDTOs.push({
        ...cartNewData,
        productPrice: data.price,
      });
      setCartsAmount((old) => old + data.price);
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + data.price)
      );
    }

    // State
    setCarts(cartOldData);
    setCartsItems((old) => old + 1);

    // LocalStorage
    localStorage.setItem("uni-cart", JSON.stringify(cartOldData));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem + 1));
  };

  // Plus One
  const addToCartPlusOne = (product) => {
    let cart = JSON.parse(JSON.stringify(carts));
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items"));
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount"));

    if (product?.discAmount !== undefined) {
      const cartIndex = cart.campaignDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.campaignDTOs[cartIndex].quantity += 1;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + product.atrDiscAmount)
      );
      setCartsAmount((old) => old + product.atrDiscAmount);
    }
    if (product?.discAmount === undefined) {
      const cartIndex = cart.orderProductsDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.orderProductsDTOs[cartIndex].quantity += 1;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount + product.price)
      );
      setCartsAmount((old) => old + product.price);
    }

    localStorage.setItem("uni-cart", JSON.stringify(cart));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem + 1));

    setCarts(cart);
    setCartsItems((old) => old + 1);
  };

  // Minus One
  const addToCartMinusOne = (product) => {
    let cart = JSON.parse(JSON.stringify(carts));
    const cartItem = JSON.parse(localStorage.getItem("uni-cart-items"));
    const cartAmount = JSON.parse(localStorage.getItem("uni-cart-amount"));

    if (product?.discAmount !== undefined) {
      const cartIndex = cart.campaignDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.campaignDTOs[cartIndex].quantity -= 1;
      const cartQuantity = cart.campaignDTOs[cartIndex].quantity;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount - product.atrDiscAmount)
      );
      setCartsAmount((old) => old - product.atrDiscAmount);
      if (cartQuantity < 1) {
        let campProdCart = cart.campaignDTOs;
        campProdCart = campProdCart.filter(
          (el) => el.productId !== product.productId
        );
        cart.campaignDTOs = campProdCart;
      }
    }
    if (product?.discAmount === undefined) {
      const cartIndex = cart.orderProductsDTOs.findIndex(
        (el) => el.productId === product.productId
      );
      cart.orderProductsDTOs[cartIndex].quantity -= 1;
      const cartQuantity = cart.orderProductsDTOs[cartIndex].quantity;
      localStorage.setItem(
        "uni-cart-amount",
        JSON.stringify(cartAmount - product.price)
      );
      setCartsAmount((old) => old - product.price);
      if (cartQuantity < 1) {
        let orderProdCart = cart.orderProductsDTOs;
        orderProdCart = orderProdCart.filter(
          (el) => el.productId !== product.productId
        );
        cart.orderProductsDTOs = orderProdCart;
      }
    }

    localStorage.setItem("uni-cart", JSON.stringify(cart));
    localStorage.setItem("uni-cart-items", JSON.stringify(cartItem - 1));

    setCarts(cart);
    setCartsItems((old) => old - 1);
  };

  //wishlist functionality

  const moveToFavorite = async (product) => {
    console.log(product);
    const data = {
      productId: product.productId,
    };
    try {
      const result = await axios({
        method: "POST",
        url: "/Customer/WishList",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: data,
      });
      notifySuccess("Moved To Wishlist!");
      console.log(result);
      setIsFavorit(true);
      setAddToFav([...addToFav, product]);
      //Cookies.set("isFavorite", JSON.stringify(true));
    } catch (error) {
      notifyError("Something Went Wrong!");
      console.error(error.response);
    }
  };

  const removeFromFavorite = async (product) => {
    const newList = addToFav.filter(
      (item) => item.productId !== product.productId
    );
    setAddToFav([...newList]);
    setIsFavorit(false);
    // let id = product.id;
    // console.log(id);
    // WishlistService.deletWishlistItem(id)
    // .then((res) => {
    //   console.log(res);
    // })
    // .catch((err) => console.log("Error occured"));
  };

  return (
    <>
      <ProductModal
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        product={product}
      />

      <div className="group box-border overflow-hidden flex rounded-md shadow-sm pe-0 flex-col items-center bg-white relative">
        <div className="relative flex justify-center w-full cursor-pointer">
          {/* {product.quantity === 0 && (
            <span className="absolute inline-flex items-center justify-center px-2 py-1 bg-red-100 text-red-600 border-0 rounded-full text-xs font-semibold font-serif z-10 left-1 top-1">
              Stock Out
            </span>
          )} */}

          <Discount product={product} />
          {router.pathname === "/offer" ? null : router.pathname ===
            "/wish-list" ? (
            <MdClose
              fontSize="22px"
              className="absolute z-10 top-1 right-1 text-green-500"
              onClick={() => removeFromFavorite(product)}
            />
          ) : // product.quantity > 0 ?
          addToFav.some((el) => el.productId === product.productId) ? (
            <MdOutlineFavorite
              fontSize="22px"
              color="#ff5b2a"
              className="absolute z-10 top-1 right-1"
              onClick={() => removeFromFavorite(product)}
            />
          ) : (
            <MdOutlineFavoriteBorder
              fontSize="22px"
              className="absolute z-10 top-1 right-1 text-green-700"
              onClick={() => moveToFavorite(product)}
            />
          )}

          <Image
            src={`https://test.bespokeit.io/${product.images[0].url}`}
            width={160}
            height={160}
            alt={product.productName}
            onClick={() => setModalOpen(!modalOpen)}
            className="object-cover transition duration-150 ease-linear transform group-hover:scale-105"
          />
        </div>
        <div className="w-full px-3 lg:px-4 pb-4 overflow-hidden">
          <div className="relative mb-1">
            <span className="text-gray-400 font-medium text-sm d-block mb-1">
              {product.weight}
            </span>
            <h2 className="text-heading truncate mb-0 block text-sm font-medium text-gray-600">
              <span className="line-clamp-2">{product.productName}</span>
            </h2>
          </div>

          <div className="flex justify-between items-center text-heading text-sm sm:text-base space-s-2 md:text-base lg:text-xl">
            <Price product={product} card={true} />
            {cartIn(product) ? (
              <div>
                <div className="h-9 w-auto flex flex-wrap items-center justify-evenly py-1 px-2 bg-green-500 text-white rounded">
                  <button onClick={() => addToCartMinusOne(product)}>
                    <span className="text-dark text-lg">
                      <IoRemove />
                    </span>
                  </button>
                  <p className="text-base text-dark px-1 font-serif font-semibold">
                    {cartOneItemQuantity(product)}
                  </p>
                  <button onClick={() => addToCartPlusOne(product)}>
                    <span className="text-dark text-lg">
                      <IoAdd />
                    </span>
                  </button>
                </div>{" "}
              </div>
            ) : (
              <button
                onClick={() => addToCartFirstTime(product)}
                aria-label="cart"
                className="h-9 w-9 flex items-center justify-center border border-gray-200 rounded text-green-500 hover:border-green-500 hover:bg-green-500 hover:text-white transition-all"
              >
                {" "}
                <span className="text-xl">
                  <IoBagAddSharp />
                </span>{" "}
              </button>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductCard;
