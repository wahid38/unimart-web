import React from "react";
import ProductCard from "@component/product/ProductCard";
import { products } from "@utils/products";
import { category } from "@utils/category";
import { useRouter } from "next/router";

export default function ProductByCayegory({ productData }) {
  //let babyCare = products.filter((item) => item.parent === category.map(i => i.parent));
  const router = useRouter();

  return (
    <div>
      {productData.map((el, i) => (
        <React.Fragment key={i}>
          {el.map((cat) => (
            <div
              key={cat.parentCatId}
              className="bg-gray-50 lg:py-0 py-2 mx-auto max-w-screen-2xl px-3 lg:pt-2 sm:px-10"
            >
              <div className="py-3 flex justify-between items-center">
                <div className="lg:w-2/5">
                  <h2 className="text-lg sm:text-xl capitalize lg:text-2xl  font-serif font-semibold">
                    {cat.parentCat}
                  </h2>
                </div>
                <div
                  onClick={() =>
                    router.push(
                      `/search/${cat.parentCatId}?Category=${cat.parentCat
                        .toLowerCase()
                        .replace("&", "")
                        .split(" ")
                        .join("-")}`
                    )
                  }
                >
                  {/* <Link href="/search?category=baby-food"> */}
                  <a className="md:text-sm leading-4 inline-flex items-center cursor-pointer transition ease-in-out duration-300 font-semibold text-center justify-center border-0 border-transparent rounded-md placeholder-white focus-visible:outline-none focus:outline-none bg-green-500 text-white px-2  py-2 md:px-5 md:py-2 hover:text-white hover:bg-green-600  text-xs lg:text-sm ">
                    Show More...
                  </a>
                  {/* </Link> */}
                </div>
              </div>

              <div className="flex">
                <div className="w-full">
                  <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-6 xl:grid-cols-6 2xl:grid-cols-6 gap-2 md:gap-3 lg:gap-3">
                    {cat.productsList.map((product, i) => (
                      <ProductCard key={product.productId} product={product} />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </React.Fragment>
      ))}
    </div>
  );
}
