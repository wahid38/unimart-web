import React, { useState } from "react";
import { IoClose } from "react-icons/io5";

export default function UserAddress({
  item,
  index,
  isSelect,
  setIsSelect,
  setSelectedAddress,
}) {
  const handleAddressCard = () => {
    setIsSelect(index);
    setSelectedAddress(item);
  };

  const removeAddress = (data) => {
    console.log(data);
  };
  return (
    <div
      onClick={() => handleAddressCard()}
      className={
        isSelect === index
          ? "border border-solid border-green-500 rounded shadow-md w-full md:w-auto h-48 px-3 pt-4 pb-1 relative cursor-pointer"
          : "border border-solid border-gray-200 rounded shadow-md w-full md:w-auto h-48 px-3 pt-4 pb-1 relative cursor-pointer"
      }
    >
      <span
        className="font-bold text-black capitalize font-serif"
        style={{ fontSize: "14px" }}
      >
        {item.name}
      </span>
      <br />
      <span className="font-normal font-serif" style={{ fontSize: "13px" }}>
        Phone : {item.contactNumber}
      </span>
      <br />
      <span className="font-normal font-serif" style={{ fontSize: "13px" }}>
        {item.houseNo},{item.roadNo}, {item.detailAddress}
      </span>
      <br />

      <span className="font-normal font-serif" style={{ fontSize: "13px" }}>
        {item.zoneName}, {item.districtName}, {item.divisionName}
      </span>
      <IoClose
        onClick={() => removeAddress(item)}
        className="absolute top-0 right-0 text-green-500 text-lg cursor-pointer"
      />

      <p
        style={{ background: "#ff5b2a" }}
        className=" absolute capitalize top-0 left-0 font-serif text-xs text-white font-semibold px-1 rounded-sm"
      >
        {item.saveAddressAs}
      </p>
    </div>
  );
}
//
//
//
//
