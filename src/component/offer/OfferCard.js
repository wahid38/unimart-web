import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { axiosGet } from "src/comon-func/AxiosGet";

const OfferCard = () => {
  const [offerData, setOfferData] = useState([]);
  const [upper, setUpper] = useState({});
  const [lower, setLower] = useState({});

  // Set Upper Lower
  useEffect(() => {
    offerData.forEach((el) => {
      if (el.position === "upper") {
        setUpper(el);
      }
      if (el.position === "lower") {
        setLower(el);
      }
    });
  }, [offerData]);

  // Page On Load
  useEffect(() => {
    axiosGet("/Carousel/getAllSideBanners", setOfferData);
  }, []);
  return (
    <div className="w-full grid grid-col md:grid-cols-2 lg:grid-cols-1 gap-4">
      <div className="block group">
        <Link href="/">
          <a>
            {" "}
            <Image
              layout="responsive"
              width={750}
              height={230}
              src={`https://test.bespokeit.io/${upper.imageUrl}`}
              alt="offer-banner"
              priority
              className="object-cover rounded-md transition duration-150 ease-linear transform group-hover:scale-105"
            />
          </a>
        </Link>
      </div>
      <div className="block group">
        <Link href="/">
          <a>
            {" "}
            <Image
              layout="responsive"
              width={750}
              height={230}
              src={`https://test.bespokeit.io/${lower.imageUrl}`}
              alt="test"
              className="object-cover rounded-md transition duration-150 ease-linear transform group-hover:scale-105"
            />
          </a>
        </Link>
      </div>
    </div>
  );
};

export default OfferCard;
